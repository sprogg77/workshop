<< ********************** >>
<<    GET NUCLEAR DATA    >>
<< ********************** >>
CLOBBER                   << overwrite files >>
GETXS 0                   << use precollapsed data from COLLAPX file >>
GETDECAY 0                << use precondensed data from ARRAYX file >>
FISPACT 
* EUROFER: DT spectrum, blanket fluence
<< ********************** >>
<<   MATERIAL DEFINITION  >>
<< ********************** >>
DENSITY 7.798             << EUROFER density in g/cm^3 >>
FUEL 57                   << nuclide (FUEL) as opposed to elemental (MASS) specification >>
B10      1.10993E+20      << number of atoms of Boron-10 in our EUROFER sample >>
B11      4.46561E+20     
C12      5.20811E+22      
C13      5.63483E+20      
N14      1.71389E+22      
N15      6.26017E+19      
O16      3.75361E+20      
O17      1.43021E+17      
O18      7.71565E+17      
Al27      8.92163E+20     
Si28      5.13707E+21     
Si29      2.60965E+20     
Si30      1.72226E+20     
P31      3.88523E+20      
S32      5.34725E+20      
S33      4.22204E+18      
S34      2.39237E+19      << number of Sulphur-34 atoms in our sample of EUROFER >>
S36      5.62916E+16      
Ti46      1.03685E+19     
Ti47      9.35053E+18     
Ti48      9.2645E+19      
Ti49      6.79895E+18     
Ti50      6.50965E+18     
V50      5.90455E+19      
V51      2.35582E+22      
Cr50     4.52416E+22      
Cr52     8.72382E+23      
Cr53     9.89238E+22      
Cr54     2.46237E+22      
Mn55    6.0221E+22                                                                                     
Fe54     5.59208E+23                                                                                   
Fe56     8.77807E+24                                                                                   
Fe57     2.02733E+23                                                                                   
Fe58     2.6979E+22                                                                                    
Co59    5.10347E+20                                                                                    
Ni58      6.97702E+20                                                                                  
Ni60      2.68746E+20                                                                                  
Ni61      1.16829E+19     
Ni62      3.72477E+19
Ni64      9.48575E+18
Cu63     1.96378E+20
Cu65     8.76095E+19
Nb93    3.23769E+20
Mo92    2.77789E+19
Mo94    1.73597E+19
Mo95    2.99051E+19
Mo96    3.1372E+19
Mo97    1.79812E+19       << these atoms all sum to 1g of EUROFER material >>
Mo98    4.54982E+19
Mo100  1.81885E+19
Ta180   4.79092E+17
Ta181   3.99222E+21
W180    4.32253E+19
W182    9.54602E+21
W183    5.15498E+21
W184    1.10375E+22
W186    1.02418E+22       
MIND 1E3
GRAPH 3 2 1               << data to plot (check GRAPH entry in manual or wiki) >> 
  1 2 3                                                                            
UNCERTAINTY 2             << enable uncertainty and pathways calculation >>
HALF
HAZARDS
<< ********************** >>
<<       IRRADATION       >>
<< ********************** >>
ATOMS                     << this keyword prints the current nuclide inventory into the output file >>
FLUX 5E+14                << turn on flux amplitude in n/(cm^2 s) >>
TIME 5 YEARS              << advance time by x years (evolving Bateman equations) >>
ATOMS                     << print nuclide inventory again >>
FLUX 0.0                  << turn off flux >>
ZERO                      << resets time to zero in anticipation of the cooling phase >>
<< ********************** >>
<<      COOLING PHASE     >>
<< ********************** >>
TIME  1.00E+01  ATOMS     << advance time by X seconds, print inventory >>
TIME  2.16E+01  ATOMS
TIME  6.84E+01  ATOMS
TIME  2.16E+02  ATOMS     
TIME  6.84E+02  ATOMS
TIME  2.16E+03  ATOMS
TIME  6.84E+03  ATOMS
TIME  2.16E+04  ATOMS
TIME  6.84E+04  ATOMS
TIME  2.16E+05  ATOMS     << continue to cool material... >>
TIME  6.84E+05  ATOMS
TIME  2.16E+06  ATOMS
TIME  6.84E+06  ATOMS
TIME  2.16E+07  ATOMS
TIME  6.84E+07  ATOMS
TIME  2.16E+08  ATOMS
TIME  6.84E+08  ATOMS
TIME  2.16E+09  ATOMS
TIME  6.84E+09  ATOMS     << ... until cooling phase is complete... terminate simulation >>
END
* END
/*
