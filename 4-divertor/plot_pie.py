import numpy as np
import matplotlib.pyplot as plt

filename = 'inventory.out'
elem_pattern = 'COMPOSITION  OF  MATERIAL  BY  ELEMENT'
elem_line_offset = 5
mass_pattern = 'INITIAL TOTAL MASS OF MATERIAL'

def find_block(lines, pattern, time_step=2, line_offset=0):
  step_counter = 0
  for line_num, line in enumerate(lines):
    if pattern in line:
      step_counter += 1
      if step_counter == time_step:
        start_line = line_num + line_offset
        break
  for line_num, line in enumerate(lines[start_line:]):
    if len(line.split()) != 11:
      end_line = line_num + start_line
      break
  return start_line, end_line

with open(filename, 'r') as f:
  lines = f.readlines()

start_line, end_line = find_block(lines, elem_pattern, time_step=2, line_offset=elem_line_offset)
for line in lines:
  if mass_pattern in line:
    init_mass = float(line.split()[6])*1000 # convert from kg to g

element = []
mass = []
data = lines[start_line:end_line]

other_m = 0
for line in data:
  words = line.split()
  m = float(words[4]) / init_mass
  e = words[1]
  if m < 0.005:
    other_m += m
    continue
  mass.append(m)
  element.append(e)
mass.append(other_m)
element.append('Other')

explode = [0.25 for entry in mass]
colors = ['wheat','lightblue','pink','blueviolet','sage','tomato']

f,ax=plt.subplots()
patches, text, autotexts = ax.pie(mass, labels=element, shadow=True, colors=colors, autopct='%1.1f%%', explode=explode, startangle=45)
for item in text:
  item.set_fontsize(14)
ax.axis('equal')
ax.set_title('Post-irradiation composition')
plt.show()

