<< ********************** >>
<<    GET NUCLEAR DATA    >>
<< ********************** >>
CLOBBER                   << overwrite files >>
GETXS 0                   << use precollapsed data from COLLAPX file >>
GETDECAY 0                << use precondensed data from ARRAYX file >>
FISPACT 
* Tungsten: DT spectrum, divertor fluence
<< ********************** >>
<<   MATERIAL DEFINITION  >>
<< ********************** >>
DENSITY 19.25             << material density in g/cm^3 >>
MASS 1.0E-3 1             << mass in kg and number of elements in following material >>
W 100.00                  << element symbol and mass percentage >>
MIND 1E3          
GRAPH 3 2 1               << data to plot (check GRAPH entry in manual or wiki) >>
  1 2 3
UNCERTAINTY 2             << enable uncertainty and pathways calculation >>
HALF
HAZARDS
<< ********************** >>
<<       IRRADATION       >>
<< ********************** >>
ATOMS                     << this keyword prints the current nuclide inventory into the output file >>
FLUX ???E+??              << turn on flux amplitude in n/(cm^2 s) >>
TIME ??? YEARS            << advance time by three years (evolving Bateman equations) >>
ATOMS                     << print nuclide inventory again >>
FLUX 0.0                  << turn off flux >>
ZERO                      << resets time to zero in anticipation of the cooling phase >>
<< ********************** >>
<<      COOLING PHASE     >>
<< ********************** >>
TIME  1.00E+01  ATOMS     << advance time by X seconds, print inventory >>
TIME  2.16E+01  ATOMS
TIME  6.84E+01  ATOMS
TIME  2.16E+02  ATOMS     
TIME  6.84E+02  ATOMS
TIME  2.16E+03  ATOMS
TIME  6.84E+03  ATOMS
TIME  2.16E+04  ATOMS
TIME  6.84E+04  ATOMS
TIME  2.16E+05  ATOMS     << continue to cool material... >>
TIME  6.84E+05  ATOMS
TIME  2.16E+06  ATOMS
TIME  6.84E+06  ATOMS
TIME  2.16E+07  ATOMS
TIME  6.84E+07  ATOMS
TIME  2.16E+08  ATOMS
TIME  6.84E+08  ATOMS
TIME  2.16E+09  ATOMS
TIME  6.84E+09  ATOMS     << ... until cooling phase is complete... terminate simulation >>
END
* END
/*
