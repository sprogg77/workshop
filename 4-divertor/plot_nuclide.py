import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib
from collections import OrderedDict

filename = 'inventory.out'
offset = 4
tiny = 1E-6
matplotlib.rcParams.update({'font.size': 16})

class Step():
  def __init__(self, number, time, start, end):
    self.number = number
    self.time = time
    self.start = start
    self.end = end
    
with open(filename, 'r') as f:
  lines = f.readlines()

# get timestep values and list of all nuclides
decay_by_step = []
t = []
nuc = []
for line_num, line in enumerate(lines):
  if 'TIME INTERVAL' in line:
    step = int(line.split()[6])
    time = float(line[50:60])
    if step < 3:
      continue
    t.append(time)
    nuc_start = line_num + offset
    for line_num, line in enumerate(lines[nuc_start:]):
      if 'TOTAL NUMBER OF NUCLIDES PRINTED IN INVENTORY' in line:
        nuc_end = nuc_start + line_num
        break
    # initialise class with time and location of data in output file
    decay_by_step.append(Step(step, time, nuc_start, nuc_end))
    for line in lines[nuc_start:nuc_end]:
      nuclide = line[2:9].replace(' ','')
      if nuclide not in nuc:
        nuc.append(nuclide)

# add total nuclide to dict
significant_list = []
for step in decay_by_step:
  step.act = {nuclide:tiny for nuclide in nuc}
  for line in lines[step.start:step.end]:
    nuclide = line[2:9].replace(' ','') 
    step.act[nuclide] = float(line[40:49])
  step.act['Total'] = np.sum(list(step.act.values()))
  # make activation dict ordered, from least active to most active (total)
  step.act = OrderedDict(sorted(step.act.items(), key=lambda x: x[1]))
  # add the second most active nuclide (i.e. first after total) to a list
  significant_list.extend(list(step.act.keys())[-4:-2])
significant_list = list(set(significant_list))
nuc.append('Total')

# take time-wise data and make nuclide-wise
decay_by_nuclide = {}
for nuclide in nuc:
  decay_arr = []
  for step in decay_by_step:
    decay_arr.append(step.act[nuclide])
  decay_by_nuclide[nuclide] = decay_arr 

# what's the maximum activity? required for thresholding nuclides
max_act = decay_by_nuclide['Total'][0]

# plot
f,ax = plt.subplots()
# get ingredients for many linestyles
linestyles = ['_', '-', '--', ':']
markers = []
for m in Line2D.markers:
    try:
        if len(m) == 1 and m != ' ':
            markers.append(m)
    except TypeError:
        pass
styles = markers + [
    r'$\lambda$',
    r'$\bowtie$',
    r'$\circlearrowleft$',
    r'$\clubsuit$',
    r'$\checkmark$']
colors = ('b', 'g', 'r', 'c', 'm', 'y', 'k')
axisNum = 0
# plot per nuclide data as a function of time
ax.plot(t, decay_by_nuclide['Total'], linestyle='dotted', color='black', label=nuclide, linewidth=5)
for nuclide in decay_by_nuclide:
  # plot nuclide if activity within 7 orders of magnitude of total at start
#  if significant(decay_by_nuclide[nuclide], threshold = 10**(np.log10(max_act) - 3)):
  if nuclide in significant_list:
    axisNum += 1
    color = colors[axisNum % len(colors)]
    if nuclide == 'Total':
      pass
    elif axisNum < len(linestyles):
      ax.plot(t, decay_by_nuclide[nuclide], linestyles[axisNum], color=color, markersize=7, label=nuclide)
    else:
      style = styles[(axisNum - len(linestyles)) % len(styles)]
      ax.plot(t, decay_by_nuclide[nuclide], marker=style, color=color, markersize=7, label=nuclide)
# make it pretty
ax.legend()
ax.grid()
ax.set_yscale('log')
ax.set_xscale('log')
ax.set_xlabel('Time [s]')
ax.set_ylabel('Activity [Bq]')
ax.set_ylim(1E3,max_act*10)
ax.set_xlim(100,max(t)*10)
ax.set_title('Post-irradiation activity')
plt.show()


      
  
