import numpy as np

def find_start_end(pattern, lines):
  start = len(lines)
  for line_num, line in enumerate(lines):
    if pattern in line:
      start = line_num + 1
    if (line_num > start) and (']' in line):
      end = line_num - 1
      break
  return start, end

def read_det(filename, det_name):
  with open(filename, 'r') as f:
    lines = f.readlines()
  pattern = 'DET' + det_name
  try:
    start, end = find_start_end(pattern, lines)
  except:
    print('Detector named: \'{}\' not found, please check.'.format(det_name))
    return
  print('Reading detector from {} in {} between lines {} and {}'.format(pattern, filename, start, end))
  values = [float(line.split()[10]) for line in lines[start:end]]
  errors = [float(line.split()[11]) for line in lines[start:end]]
  return np.array(values), np.array(errors)

def read_ene(filename, det_name):
  with open(filename, 'r') as f:
    lines = f.readlines()
  pattern = 'DET' + det_name + 'E'
  try:
    start, end = find_start_end(pattern, lines)
  except:
    print('Detector named: \'{}\' not found, please check.'.format(det_name))
    return
  print('Reading energy grid from {} in {} between lines {} and {}'.format(pattern, filename, start, end))
  e_min = [float(line.split()[0]) for line in lines[start:end]]
  e_max = [float(line.split()[1]) for line in lines[start:end]]
  e_mean = [float(line.split()[2]) for line in lines[start:end]]
  return np.array(e_min), np.array(e_max), np.array(e_mean)
