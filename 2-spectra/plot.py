from read import read_det, read_ene
import matplotlib.pyplot as plt

det_name = 'phi'

# read detector flux as a function of energy
# filename is first argument, then detector name as defined in input file
phi, phi_error = read_det('spheres.serp_det0.m', det_name) 
# read energy grid (each bin has lower and upper bounds, and a mean value)
# arguments as above
e_min, e_max, e_mean = read_ene('spheres.serp_det0.m', det_name)

# create figure and axes to plot on
f,ax=plt.subplots()
# plot stepwise function of flux against energy
ax.step(e_max,phi,label=det_name)
# make it nice
ax.set_yscale('log')
ax.set_xscale('log')
ax.legend()
plt.show()
