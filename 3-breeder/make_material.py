#!/usr/bin/env python3

# Material maker and mixer by Jonathan Shimwell Jonathan.Shimwell@ukaea.uk
# This program untested so use it at your own risk
# Please let me know if you spot any bugs
# If optimising material compositions is of interest I hope to offer a project on topological optimisations of central column shielding.
# Please contact me for more details

import re
import sys

sys.dont_write_bytecode = True

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


class Isotope:
    def __init__(self, symbol_or_proton, atomic_number, *abundance):
        if type(symbol_or_proton) == int or symbol_or_proton.isdigit():
            self.symbol = self.find_symbol_from_protons(symbol_or_proton)
        else:
            self.symbol = symbol_or_proton
        self.atomic_number = atomic_number
        try:
            self.abundance = abundance[0]
        except:
            self.abundance = self.natural_abundance



    @property
    def natural_abundance(self):
        if self.symbol == 'H' and self.atomic_number == 1: return 0.999885
        if self.symbol == 'H' and self.atomic_number == 2: return 0.000115
        if self.symbol == 'He' and self.atomic_number == 3: return 0.00000134
        if self.symbol == 'He' and self.atomic_number == 4: return 0.99999866
        if self.symbol == 'Li' and self.atomic_number == 6: return 0.0759
        if self.symbol == 'Li' and self.atomic_number == 7: return 0.9241
        if self.symbol == 'Be' and self.atomic_number == 9: return 1
        if self.symbol == 'B' and self.atomic_number == 10: return 0.199
        if self.symbol == 'B' and self.atomic_number == 11: return 0.801
        if self.symbol == 'C' and self.atomic_number == 12: return 0.9893
        if self.symbol == 'C' and self.atomic_number == 13: return 0.0107
        if self.symbol == 'C' and self.atomic_number == 14: return 0
        if self.symbol == 'N' and self.atomic_number == 14: return 0.99636
        if self.symbol == 'N' and self.atomic_number == 15: return 0.00364
        if self.symbol == 'O' and self.atomic_number == 16: return 1
        if self.symbol == 'F' and self.atomic_number == 19: return 1
        if self.symbol == 'Ne' and self.atomic_number == 20: return 0.9048
        if self.symbol == 'Ne' and self.atomic_number == 21: return 0.0027
        if self.symbol == 'Ne' and self.atomic_number == 22: return 0.0925
        if self.symbol == 'Na' and self.atomic_number == 23: return 1
        if self.symbol == 'Mg' and self.atomic_number == 24: return 0.7899
        if self.symbol == 'Mg' and self.atomic_number == 25: return 0.1
        if self.symbol == 'Mg' and self.atomic_number == 26: return 0.1101
        if self.symbol == 'Al' and self.atomic_number == 27: return 1
        if self.symbol == 'Si' and self.atomic_number == 28: return 0.92223
        if self.symbol == 'Si' and self.atomic_number == 29: return 0.04685
        if self.symbol == 'Si' and self.atomic_number == 30: return 0.03092
        if self.symbol == 'P' and self.atomic_number == 31: return 1
        if self.symbol == 'S' and self.atomic_number == 32: return 0.9499
        if self.symbol == 'S' and self.atomic_number == 33: return 0.0075
        if self.symbol == 'S' and self.atomic_number == 34: return 0.0425
        if self.symbol == 'S' and self.atomic_number == 36: return 0.0001
        if self.symbol == 'Cl' and self.atomic_number == 35: return 0.7576
        if self.symbol == 'Cl' and self.atomic_number == 37: return 0.2424
        if self.symbol == 'Ar' and self.atomic_number == 36: return 0.003336
        if self.symbol == 'Ar' and self.atomic_number == 38: return 0.000629
        if self.symbol == 'Ar' and self.atomic_number == 40: return 0.996035
        if self.symbol == 'K' and self.atomic_number == 39: return 0.932581
        if self.symbol == 'K' and self.atomic_number == 40: return 0.000117
        if self.symbol == 'K' and self.atomic_number == 41: return 0.067302
        if self.symbol == 'Ca' and self.atomic_number == 40: return 0.96941
        if self.symbol == 'Ca' and self.atomic_number == 42: return 0.00647
        if self.symbol == 'Ca' and self.atomic_number == 43: return 0.00135
        if self.symbol == 'Ca' and self.atomic_number == 44: return 0.02086
        if self.symbol == 'Ca' and self.atomic_number == 46: return 0.00004
        if self.symbol == 'Ca' and self.atomic_number == 48: return 0.00187
        if self.symbol == 'Sc' and self.atomic_number == 45: return 1
        if self.symbol == 'Ti' and self.atomic_number == 46: return 0.0825
        if self.symbol == 'Ti' and self.atomic_number == 47: return 0.0744
        if self.symbol == 'Ti' and self.atomic_number == 48: return 0.7372
        if self.symbol == 'Ti' and self.atomic_number == 49: return 0.0541
        if self.symbol == 'Ti' and self.atomic_number == 50: return 0.0518
        if self.symbol == 'V' and self.atomic_number == 50: return 0.0025
        if self.symbol == 'V' and self.atomic_number == 51: return 0.9975
        if self.symbol == 'Cr' and self.atomic_number == 50: return 0.04345
        if self.symbol == 'Cr' and self.atomic_number == 52: return 0.83789
        if self.symbol == 'Cr' and self.atomic_number == 53: return 0.09501
        if self.symbol == 'Cr' and self.atomic_number == 54: return 0.02365
        if self.symbol == 'Mn' and self.atomic_number == 55: return 1
        if self.symbol == 'Fe' and self.atomic_number == 54: return 0.05845
        if self.symbol == 'Fe' and self.atomic_number == 56: return 0.91754
        if self.symbol == 'Fe' and self.atomic_number == 57: return 0.02119
        if self.symbol == 'Fe' and self.atomic_number == 58: return 0.00282
        if self.symbol == 'Co' and self.atomic_number == 59: return 1
        if self.symbol == 'Ni' and self.atomic_number == 58: return 0.68077
        if self.symbol == 'Ni' and self.atomic_number == 60: return 0.26223
        if self.symbol == 'Ni' and self.atomic_number == 61: return 0.011399
        if self.symbol == 'Ni' and self.atomic_number == 62: return 0.036346
        if self.symbol == 'Ni' and self.atomic_number == 64: return 0.009255
        if self.symbol == 'Cu' and self.atomic_number == 63: return 0.6915
        if self.symbol == 'Cu' and self.atomic_number == 65: return 0.3085
        if self.symbol == 'Zn' and self.atomic_number == 64: return 0.4917
        if self.symbol == 'Zn' and self.atomic_number == 66: return 0.2773
        if self.symbol == 'Zn' and self.atomic_number == 67: return 0.0404
        if self.symbol == 'Zn' and self.atomic_number == 68: return 0.1845
        if self.symbol == 'Zn' and self.atomic_number == 70: return 0.0061
        if self.symbol == 'Ga' and self.atomic_number == 69: return 0.60108
        if self.symbol == 'Ga' and self.atomic_number == 71: return 0.39892
        if self.symbol == 'Ge' and self.atomic_number == 70: return 0.2057
        if self.symbol == 'Ge' and self.atomic_number == 72: return 0.2745
        if self.symbol == 'Ge' and self.atomic_number == 73: return 0.0775
        if self.symbol == 'Ge' and self.atomic_number == 74: return 0.365
        if self.symbol == 'Ge' and self.atomic_number == 76: return 0.0773
        if self.symbol == 'As' and self.atomic_number == 75: return 1
        if self.symbol == 'Se' and self.atomic_number == 74: return 0.0089
        if self.symbol == 'Se' and self.atomic_number == 76: return 0.0937
        if self.symbol == 'Se' and self.atomic_number == 77: return 0.0763
        if self.symbol == 'Se' and self.atomic_number == 78: return 0.2377
        if self.symbol == 'Se' and self.atomic_number == 80: return 0.4961
        if self.symbol == 'Se' and self.atomic_number == 82: return 0.0873
        if self.symbol == 'Br' and self.atomic_number == 79: return 0.5069
        if self.symbol == 'Br' and self.atomic_number == 81: return 0.4931
        if self.symbol == 'Kr' and self.atomic_number == 78: return 0.00355
        if self.symbol == 'Kr' and self.atomic_number == 80: return 0.02286
        if self.symbol == 'Kr' and self.atomic_number == 82: return 0.11593
        if self.symbol == 'Kr' and self.atomic_number == 83: return 0.115
        if self.symbol == 'Kr' and self.atomic_number == 84: return 0.56987
        if self.symbol == 'Kr' and self.atomic_number == 86: return 0.17279
        if self.symbol == 'Rb' and self.atomic_number == 85: return 0.7217
        if self.symbol == 'Rb' and self.atomic_number == 87: return 0.2783
        if self.symbol == 'Sr' and self.atomic_number == 84: return 0.0056
        if self.symbol == 'Sr' and self.atomic_number == 86: return 0.0986
        if self.symbol == 'Sr' and self.atomic_number == 87: return 0.07
        if self.symbol == 'Sr' and self.atomic_number == 88: return 0.8258
        if self.symbol == 'Y' and self.atomic_number == 89: return 1
        if self.symbol == 'Zr' and self.atomic_number == 90: return 0.5145
        if self.symbol == 'Zr' and self.atomic_number == 91: return 0.1122
        if self.symbol == 'Zr' and self.atomic_number == 92: return 0.1715
        if self.symbol == 'Zr' and self.atomic_number == 94: return 0.1738
        if self.symbol == 'Zr' and self.atomic_number == 96: return 0.028
        if self.symbol == 'Nb' and self.atomic_number == 93: return 1
        if self.symbol == 'Mo' and self.atomic_number == 92: return 0.1453
        if self.symbol == 'Mo' and self.atomic_number == 94: return 0.0915
        if self.symbol == 'Mo' and self.atomic_number == 95: return 0.1584
        if self.symbol == 'Mo' and self.atomic_number == 96: return 0.1667
        if self.symbol == 'Mo' and self.atomic_number == 97: return 0.096
        if self.symbol == 'Mo' and self.atomic_number == 98: return 0.2439
        if self.symbol == 'Mo' and self.atomic_number == 100: return 0.0982
        if self.symbol == 'Ru' and self.atomic_number == 96: return 0.0554
        if self.symbol == 'Ru' and self.atomic_number == 98: return 0.0187
        if self.symbol == 'Ru' and self.atomic_number == 99: return 0.1276
        if self.symbol == 'Ru' and self.atomic_number == 100: return 0.126
        if self.symbol == 'Ru' and self.atomic_number == 101: return 0.1706
        if self.symbol == 'Ru' and self.atomic_number == 102: return 0.3155
        if self.symbol == 'Ru' and self.atomic_number == 104: return 0.1862
        if self.symbol == 'Rh' and self.atomic_number == 103: return 1
        if self.symbol == 'Pd' and self.atomic_number == 102: return 0.0102
        if self.symbol == 'Pd' and self.atomic_number == 104: return 0.1114
        if self.symbol == 'Pd' and self.atomic_number == 105: return 0.2233
        if self.symbol == 'Pd' and self.atomic_number == 106: return 0.2733
        if self.symbol == 'Pd' and self.atomic_number == 108: return 0.2646
        if self.symbol == 'Pd' and self.atomic_number == 110: return 0.1172
        if self.symbol == 'Ag' and self.atomic_number == 107: return 0.51839
        if self.symbol == 'Ag' and self.atomic_number == 109: return 0.48161
        if self.symbol == 'Cd' and self.atomic_number == 106: return 0.0125
        if self.symbol == 'Cd' and self.atomic_number == 108: return 0.0089
        if self.symbol == 'Cd' and self.atomic_number == 110: return 0.1249
        if self.symbol == 'Cd' and self.atomic_number == 111: return 0.128
        if self.symbol == 'Cd' and self.atomic_number == 112: return 0.2413
        if self.symbol == 'Cd' and self.atomic_number == 113: return 0.1222
        if self.symbol == 'Cd' and self.atomic_number == 114: return 0.2873
        if self.symbol == 'Cd' and self.atomic_number == 116: return 0.0749
        if self.symbol == 'In' and self.atomic_number == 113: return 0.0429
        if self.symbol == 'In' and self.atomic_number == 115: return 0.9571
        if self.symbol == 'Sn' and self.atomic_number == 112: return 0.0097
        if self.symbol == 'Sn' and self.atomic_number == 114: return 0.0066
        if self.symbol == 'Sn' and self.atomic_number == 115: return 0.0034
        if self.symbol == 'Sn' and self.atomic_number == 116: return 0.1454
        if self.symbol == 'Sn' and self.atomic_number == 117: return 0.0768
        if self.symbol == 'Sn' and self.atomic_number == 118: return 0.2422
        if self.symbol == 'Sn' and self.atomic_number == 119: return 0.0859
        if self.symbol == 'Sn' and self.atomic_number == 120: return 0.3258
        if self.symbol == 'Sn' and self.atomic_number == 122: return 0.0463
        if self.symbol == 'Sn' and self.atomic_number == 124: return 0.0579
        if self.symbol == 'Sb' and self.atomic_number == 121: return 0.5721
        if self.symbol == 'Sb' and self.atomic_number == 123: return 0.4279
        if self.symbol == 'Te' and self.atomic_number == 120: return 0.0009
        if self.symbol == 'Te' and self.atomic_number == 122: return 0.0255
        if self.symbol == 'Te' and self.atomic_number == 123: return 0.0089
        if self.symbol == 'Te' and self.atomic_number == 124: return 0.0474
        if self.symbol == 'Te' and self.atomic_number == 125: return 0.0707
        if self.symbol == 'Te' and self.atomic_number == 126: return 0.1884
        if self.symbol == 'Te' and self.atomic_number == 128: return 0.3174
        if self.symbol == 'Te' and self.atomic_number == 130: return 0.3408
        if self.symbol == 'I' and self.atomic_number == 127: return 1
        if self.symbol == 'Xe' and self.atomic_number == 124: return 0.000952
        if self.symbol == 'Xe' and self.atomic_number == 126: return 0.00089
        if self.symbol == 'Xe' and self.atomic_number == 128: return 0.019102
        if self.symbol == 'Xe' and self.atomic_number == 129: return 0.264006
        if self.symbol == 'Xe' and self.atomic_number == 130: return 0.04071
        if self.symbol == 'Xe' and self.atomic_number == 131: return 0.212324
        if self.symbol == 'Xe' and self.atomic_number == 132: return 0.269086
        if self.symbol == 'Xe' and self.atomic_number == 134: return 0.104357
        if self.symbol == 'Xe' and self.atomic_number == 136: return 0.088573
        if self.symbol == 'Cs' and self.atomic_number == 133: return 1
        if self.symbol == 'Ba' and self.atomic_number == 130: return 0.00106
        if self.symbol == 'Ba' and self.atomic_number == 132: return 0.00101
        if self.symbol == 'Ba' and self.atomic_number == 134: return 0.02417
        if self.symbol == 'Ba' and self.atomic_number == 135: return 0.06592
        if self.symbol == 'Ba' and self.atomic_number == 136: return 0.07854
        if self.symbol == 'Ba' and self.atomic_number == 137: return 0.11232
        if self.symbol == 'Ba' and self.atomic_number == 138: return 0.71698
        if self.symbol == 'La' and self.atomic_number == 138: return 0.0008881
        if self.symbol == 'La' and self.atomic_number == 139: return 0.9991119
        if self.symbol == 'Ce' and self.atomic_number == 136: return 0.00185
        if self.symbol == 'Ce' and self.atomic_number == 138: return 0.00251
        if self.symbol == 'Ce' and self.atomic_number == 140: return 0.8845
        if self.symbol == 'Ce' and self.atomic_number == 142: return 0.11114
        if self.symbol == 'Pr' and self.atomic_number == 141: return 1
        if self.symbol == 'Nd' and self.atomic_number == 142: return 0.27152
        if self.symbol == 'Nd' and self.atomic_number == 143: return 0.12174
        if self.symbol == 'Nd' and self.atomic_number == 144: return 0.23798
        if self.symbol == 'Nd' and self.atomic_number == 145: return 0.08293
        if self.symbol == 'Nd' and self.atomic_number == 146: return 0.17189
        if self.symbol == 'Nd' and self.atomic_number == 148: return 0.05756
        if self.symbol == 'Nd' and self.atomic_number == 150: return 0.05638
        if self.symbol == 'Sm' and self.atomic_number == 144: return 0.0307
        if self.symbol == 'Sm' and self.atomic_number == 147: return 0.1499
        if self.symbol == 'Sm' and self.atomic_number == 148: return 0.1124
        if self.symbol == 'Sm' and self.atomic_number == 149: return 0.1382
        if self.symbol == 'Sm' and self.atomic_number == 150: return 0.0738
        if self.symbol == 'Sm' and self.atomic_number == 152: return 0.2675
        if self.symbol == 'Sm' and self.atomic_number == 154: return 0.2275
        if self.symbol == 'Eu' and self.atomic_number == 151: return 0.4781
        if self.symbol == 'Eu' and self.atomic_number == 153: return 0.5219
        if self.symbol == 'Gd' and self.atomic_number == 152: return 0.002
        if self.symbol == 'Gd' and self.atomic_number == 154: return 0.0218
        if self.symbol == 'Gd' and self.atomic_number == 155: return 0.148
        if self.symbol == 'Gd' and self.atomic_number == 156: return 0.2047
        if self.symbol == 'Gd' and self.atomic_number == 157: return 0.1565
        if self.symbol == 'Gd' and self.atomic_number == 158: return 0.2484
        if self.symbol == 'Gd' and self.atomic_number == 160: return 0.2186
        if self.symbol == 'Tb' and self.atomic_number == 159: return 1
        if self.symbol == 'Dy' and self.atomic_number == 156: return 0.00056
        if self.symbol == 'Dy' and self.atomic_number == 158: return 0.00095
        if self.symbol == 'Dy' and self.atomic_number == 160: return 0.02329
        if self.symbol == 'Dy' and self.atomic_number == 161: return 0.18889
        if self.symbol == 'Dy' and self.atomic_number == 162: return 0.25475
        if self.symbol == 'Dy' and self.atomic_number == 163: return 0.24896
        if self.symbol == 'Dy' and self.atomic_number == 164: return 0.2826
        if self.symbol == 'Ho' and self.atomic_number == 165: return 1
        if self.symbol == 'Er' and self.atomic_number == 162: return 0.00139
        if self.symbol == 'Er' and self.atomic_number == 164: return 0.01601
        if self.symbol == 'Er' and self.atomic_number == 166: return 0.33503
        if self.symbol == 'Er' and self.atomic_number == 167: return 0.22869
        if self.symbol == 'Er' and self.atomic_number == 168: return 0.26978
        if self.symbol == 'Er' and self.atomic_number == 170: return 0.1491
        if self.symbol == 'Tm' and self.atomic_number == 169: return 1
        if self.symbol == 'Yb' and self.atomic_number == 168: return 0.00123
        if self.symbol == 'Yb' and self.atomic_number == 170: return 0.02982
        if self.symbol == 'Yb' and self.atomic_number == 171: return 0.1409
        if self.symbol == 'Yb' and self.atomic_number == 172: return 0.2168
        if self.symbol == 'Yb' and self.atomic_number == 173: return 0.16103
        if self.symbol == 'Yb' and self.atomic_number == 174: return 0.32026
        if self.symbol == 'Yb' and self.atomic_number == 176: return 0.12996
        if self.symbol == 'Lu' and self.atomic_number == 175: return 0.97401
        if self.symbol == 'Lu' and self.atomic_number == 176: return 0.02599
        if self.symbol == 'Hf' and self.atomic_number == 174: return 0.0016
        if self.symbol == 'Hf' and self.atomic_number == 176: return 0.0526
        if self.symbol == 'Hf' and self.atomic_number == 177: return 0.186
        if self.symbol == 'Hf' and self.atomic_number == 178: return 0.2728
        if self.symbol == 'Hf' and self.atomic_number == 179: return 0.1362
        if self.symbol == 'Hf' and self.atomic_number == 180: return 0.3508
        if self.symbol == 'Ta' and self.atomic_number == 180: return 0.0001201
        if self.symbol == 'Ta' and self.atomic_number == 181: return 0.9998799
        if self.symbol == 'W' and self.atomic_number == 180: return 0.0012
        if self.symbol == 'W' and self.atomic_number == 182: return 0.265
        if self.symbol == 'W' and self.atomic_number == 183: return 0.1431
        if self.symbol == 'W' and self.atomic_number == 184: return 0.3064
        if self.symbol == 'W' and self.atomic_number == 186: return 0.2843
        if self.symbol == 'Re' and self.atomic_number == 185: return 0.374
        if self.symbol == 'Re' and self.atomic_number == 187: return 0.626
        if self.symbol == 'Os' and self.atomic_number == 184: return 0.0002
        if self.symbol == 'Os' and self.atomic_number == 186: return 0.0159
        if self.symbol == 'Os' and self.atomic_number == 187: return 0.0196
        if self.symbol == 'Os' and self.atomic_number == 188: return 0.1324
        if self.symbol == 'Os' and self.atomic_number == 189: return 0.1615
        if self.symbol == 'Os' and self.atomic_number == 190: return 0.2626
        if self.symbol == 'Os' and self.atomic_number == 192: return 0.4078
        if self.symbol == 'Ir' and self.atomic_number == 191: return 0.373
        if self.symbol == 'Ir' and self.atomic_number == 193: return 0.627
        if self.symbol == 'Pt' and self.atomic_number == 190: return 0.00012
        if self.symbol == 'Pt' and self.atomic_number == 192: return 0.00782
        if self.symbol == 'Pt' and self.atomic_number == 194: return 0.3286
        if self.symbol == 'Pt' and self.atomic_number == 195: return 0.3378
        if self.symbol == 'Pt' and self.atomic_number == 196: return 0.2521
        if self.symbol == 'Pt' and self.atomic_number == 198: return 0.07356
        if self.symbol == 'Au' and self.atomic_number == 197: return 1
        if self.symbol == 'Hg' and self.atomic_number == 196: return 0.0015
        if self.symbol == 'Hg' and self.atomic_number == 198: return 0.0997
        if self.symbol == 'Hg' and self.atomic_number == 199: return 0.1687
        if self.symbol == 'Hg' and self.atomic_number == 200: return 0.231
        if self.symbol == 'Hg' and self.atomic_number == 201: return 0.1318
        if self.symbol == 'Hg' and self.atomic_number == 202: return 0.2986
        if self.symbol == 'Hg' and self.atomic_number == 204: return 0.0687
        if self.symbol == 'Tl' and self.atomic_number == 203: return 0.2952
        if self.symbol == 'Tl' and self.atomic_number == 205: return 0.7048
        if self.symbol == 'Pb' and self.atomic_number == 204: return 0.014
        if self.symbol == 'Pb' and self.atomic_number == 206: return 0.241
        if self.symbol == 'Pb' and self.atomic_number == 207: return 0.221
        if self.symbol == 'Pb' and self.atomic_number == 208: return 0.524
        if self.symbol == 'Bi' and self.atomic_number == 209: return 1
        if self.symbol == 'Th' and self.atomic_number == 232: return 1
        if self.symbol == 'Pa' and self.atomic_number == 231: return 1
        if self.symbol == 'U' and self.atomic_number == 234: return 0.000054
        if self.symbol == 'U' and self.atomic_number == 235: return 0.007204
        if self.symbol == 'U' and self.atomic_number == 238: return 0.992742

    @property
    def neutrons(self):
        return self.atomic_number - self.protons


    def find_symbol_from_protons(self,protons):
        if protons ==  1 : return 'H'
        if protons ==  2 : return 'He'
        if protons ==  3 : return 'Li'
        if protons ==  4 : return 'Be'
        if protons ==  5 : return 'B'
        if protons ==  6 : return 'C'
        if protons ==  7 : return 'N'
        if protons ==  8 : return 'O'
        if protons ==  9 : return 'F'
        if protons ==  10 : return 'Ne'
        if protons ==  11 : return 'Na'
        if protons ==  12 : return 'Mg'
        if protons ==  13 : return 'Al'
        if protons ==  14 : return 'Si'
        if protons ==  15 : return 'P'
        if protons ==  16 : return 'S'
        if protons ==  17 : return 'Cl'
        if protons ==  18 : return 'Ar'
        if protons ==  19 : return 'K'
        if protons ==  20 : return 'Ca'
        if protons ==  21 : return 'Sc'
        if protons ==  22 : return 'Ti'
        if protons ==  23 : return 'V'
        if protons ==  24 : return 'Cr'
        if protons ==  25 : return 'Mn'
        if protons ==  26 : return 'Fe'
        if protons ==  27 : return 'Co'
        if protons ==  28 : return 'Ni'
        if protons ==  29 : return 'Cu'
        if protons ==  30 : return 'Zn'
        if protons ==  31 : return 'Ga'
        if protons ==  32 : return 'Ge'
        if protons ==  33 : return 'As'
        if protons ==  34 : return 'Se'
        if protons ==  35 : return 'Br'
        if protons ==  36 : return 'Kr'
        if protons ==  37 : return 'Rb'
        if protons ==  38 : return 'Sr'
        if protons ==  39 : return 'Y'
        if protons ==  40 : return 'Zr'
        if protons ==  41 : return 'Nb'
        if protons ==  42 : return 'Mo'
        if protons ==  43 : return 'Tc'
        if protons ==  44 : return 'Ru'
        if protons ==  45 : return 'Rh'
        if protons ==  46 : return 'Pd'
        if protons ==  47 : return 'Ag'
        if protons ==  48 : return 'Cd'
        if protons ==  49 : return 'In'
        if protons ==  50 : return 'Sn'
        if protons ==  51 : return 'Sb'
        if protons ==  52 : return 'Te'
        if protons ==  53 : return 'I'
        if protons ==  54 : return 'Xe'
        if protons ==  55 : return 'Cs'
        if protons ==  56 : return 'Ba'
        if protons ==  57 : return 'La'
        if protons ==  58 : return 'Ce'
        if protons ==  59 : return 'Pr'
        if protons ==  60 : return 'Nd'
        if protons ==  61 : return 'Pm'
        if protons ==  62 : return 'Sm'
        if protons ==  63 : return 'Eu'
        if protons ==  64 : return 'Gd'
        if protons ==  65 : return 'Tb'
        if protons ==  66 : return 'Dy'
        if protons ==  67 : return 'Ho'
        if protons ==  68 : return 'Er'
        if protons ==  69 : return 'Tm'
        if protons ==  70 : return 'Yb'
        if protons ==  71 : return 'Lu'
        if protons ==  72 : return 'Hf'
        if protons ==  73 : return 'Ta'
        if protons ==  74 : return 'W'
        if protons ==  75 : return 'Re'
        if protons ==  76 : return 'Os'
        if protons ==  77 : return 'Ir'
        if protons ==  78 : return 'Pt'
        if protons ==  79 : return 'Au'
        if protons ==  80 : return 'Hg'
        if protons ==  81 : return 'Tl'
        if protons ==  82 : return 'Pb'
        if protons ==  83 : return 'Bi'
        if protons ==  84 : return 'Po'
        if protons ==  85 : return 'At'
        if protons ==  86 : return 'Rn'
        if protons ==  87 : return 'Fr'
        if protons ==  88 : return 'Ra'
        if protons ==  89 : return 'Ac'
        if protons ==  90 : return 'Th'
        if protons ==  91 : return 'Pa'
        if protons ==  92 : return 'U'
        if protons ==  93 : return 'Np'
        if protons ==  94 : return 'Pu'
        if protons ==  95 : return 'Am'
        if protons ==  96 : return 'Cm'
        if protons ==  97 : return 'Bk'
        if protons ==  98 : return 'Cf'
        if protons ==  99 : return 'Es'
        if protons ==  100 : return 'Fm'
        if protons ==  101 : return 'Md'
        if protons ==  102 : return 'No'
        if protons ==  103 : return 'Lr'
        if protons ==  104 : return 'Rf'
        if protons ==  105 : return 'Db'
        if protons ==  106 : return 'Sg'
        if protons ==  107 : return 'Bh'
        if protons ==  108 : return 'Hs'
        if protons ==  109 : return 'Mt'
        if protons ==  110 : return 'Ds'
        if protons ==  111 : return 'Rg'
        if protons ==  112 : return 'Cn'
        if protons ==  113 : return 'Nh'
        if protons ==  114 : return 'Fl'
        if protons ==  115 : return 'Mc'
        if protons ==  116 : return 'Lv'
        if protons ==  117 : return 'Ts'
        if protons ==  118 : return 'Og'
        return ('proton number ' + str(protons) + ' not found')

    @property
    def protons(self):
        if self.symbol == 'H': return 1
        if self.symbol == 'He': return 2
        if self.symbol == 'Li': return 3
        if self.symbol == 'Be': return 4
        if self.symbol == 'B': return 5
        if self.symbol == 'C': return 6
        if self.symbol == 'N': return 7
        if self.symbol == 'O': return 8
        if self.symbol == 'F': return 9
        if self.symbol == 'Ne': return 10
        if self.symbol == 'Na': return 11
        if self.symbol == 'Mg': return 12
        if self.symbol == 'Al': return 13
        if self.symbol == 'Si': return 14
        if self.symbol == 'P': return 15
        if self.symbol == 'S': return 16
        if self.symbol == 'Cl': return 17
        if self.symbol == 'Ar': return 18
        if self.symbol == 'K': return 19
        if self.symbol == 'Ca': return 20
        if self.symbol == 'Sc': return 21
        if self.symbol == 'Ti': return 22
        if self.symbol == 'V': return 23
        if self.symbol == 'Cr': return 24
        if self.symbol == 'Mn': return 25
        if self.symbol == 'Fe': return 26
        if self.symbol == 'Co': return 27
        if self.symbol == 'Ni': return 28
        if self.symbol == 'Cu': return 29
        if self.symbol == 'Zn': return 30
        if self.symbol == 'Ga': return 31
        if self.symbol == 'Ge': return 32
        if self.symbol == 'As': return 33
        if self.symbol == 'Se': return 34
        if self.symbol == 'Br': return 35
        if self.symbol == 'Kr': return 36
        if self.symbol == 'Rb': return 37
        if self.symbol == 'Sr': return 38
        if self.symbol == 'Y': return 39
        if self.symbol == 'Zr': return 40
        if self.symbol == 'Nb': return 41
        if self.symbol == 'Mo': return 42
        if self.symbol == 'Tc': return 43
        if self.symbol == 'Ru': return 44
        if self.symbol == 'Rh': return 45
        if self.symbol == 'Pd': return 46
        if self.symbol == 'Ag': return 47
        if self.symbol == 'Cd': return 48
        if self.symbol == 'In': return 49
        if self.symbol == 'Sn': return 50
        if self.symbol == 'Sb': return 51
        if self.symbol == 'Te': return 52
        if self.symbol == 'I': return 53
        if self.symbol == 'Xe': return 54
        if self.symbol == 'Cs': return 55
        if self.symbol == 'Ba': return 56
        if self.symbol == 'La': return 57
        if self.symbol == 'Ce': return 58
        if self.symbol == 'Pr': return 59
        if self.symbol == 'Nd': return 60
        if self.symbol == 'Pm': return 61
        if self.symbol == 'Sm': return 62
        if self.symbol == 'Eu': return 63
        if self.symbol == 'Gd': return 64
        if self.symbol == 'Tb': return 65
        if self.symbol == 'Dy': return 66
        if self.symbol == 'Ho': return 67
        if self.symbol == 'Er': return 68
        if self.symbol == 'Tm': return 69
        if self.symbol == 'Yb': return 70
        if self.symbol == 'Lu': return 71
        if self.symbol == 'Hf': return 72
        if self.symbol == 'Ta': return 73
        if self.symbol == 'W': return 74
        if self.symbol == 'Re': return 75
        if self.symbol == 'Os': return 76
        if self.symbol == 'Ir': return 77
        if self.symbol == 'Pt': return 78
        if self.symbol == 'Au': return 79
        if self.symbol == 'Hg': return 80
        if self.symbol == 'Tl': return 81
        if self.symbol == 'Pb': return 82
        if self.symbol == 'Bi': return 83
        if self.symbol == 'Po': return 84
        if self.symbol == 'At': return 85
        if self.symbol == 'Rn': return 86
        if self.symbol == 'Fr': return 87
        if self.symbol == 'Ra': return 88
        if self.symbol == 'Ac': return 89
        if self.symbol == 'Th': return 90
        if self.symbol == 'Pa': return 91
        if self.symbol == 'U': return 92
        if self.symbol == 'Np': return 93
        if self.symbol == 'Pu': return 94
        if self.symbol == 'Am': return 95
        if self.symbol == 'Cm': return 96
        if self.symbol == 'Bk': return 97
        if self.symbol == 'Cf': return 98
        if self.symbol == 'Es': return 99
        if self.symbol == 'Fm': return 100
        if self.symbol == 'Md': return 101
        if self.symbol == 'No': return 102
        if self.symbol == 'Lr': return 103
        if self.symbol == 'Rf': return 104
        if self.symbol == 'Db': return 105
        if self.symbol == 'Sg': return 106
        if self.symbol == 'Bh': return 107
        if self.symbol == 'Hs': return 108
        if self.symbol == 'Mt': return 109
        if self.symbol == 'Ds': return 110
        if self.symbol == 'Rg': return 111
        if self.symbol == 'Cn': return 112
        if self.symbol == 'Nh': return 113
        if self.symbol == 'Fl': return 114
        if self.symbol == 'Mc': return 115
        if self.symbol == 'Lv': return 116
        if self.symbol == 'Ts': return 117
        if self.symbol == 'Og': return 118
        return ('proton number for ' + self.symbol + ' not found')

    @property
    def mass_amu(self):
        if self.symbol == 'H' and self.atomic_number == 1: return 1.0078250322
        if self.symbol == 'H' and self.atomic_number == 2: return 2.0141017781
        if self.symbol == 'H' and self.atomic_number == 3: return 3.0160492779
        if self.symbol == 'He' and self.atomic_number == 3: return 3.0160293201
        if self.symbol == 'He' and self.atomic_number == 4: return 4.0026032541
        if self.symbol == 'Li' and self.atomic_number == 6: return 6.0151228874
        if self.symbol == 'Li' and self.atomic_number == 7: return 7.0160034366
        if self.symbol == 'Be' and self.atomic_number == 9: return 9.012183065
        if self.symbol == 'B' and self.atomic_number == 10: return 10.01293695
        if self.symbol == 'B' and self.atomic_number == 11: return 11.00930536
        if self.symbol == 'C' and self.atomic_number == 12: return 12
        if self.symbol == 'C' and self.atomic_number == 13: return 13.0033548351
        if self.symbol == 'C' and self.atomic_number == 14: return 14.0032419884
        if self.symbol == 'N' and self.atomic_number == 14: return 14.0030740044
        if self.symbol == 'N' and self.atomic_number == 15: return 15.0001088989
        if self.symbol == 'O' and self.atomic_number == 16: return 15.9949146196
        if self.symbol == 'F' and self.atomic_number == 19: return 18.9984031627
        if self.symbol == 'Ne' and self.atomic_number == 20: return 19.9924401762
        if self.symbol == 'Ne' and self.atomic_number == 21: return 20.993846685
        if self.symbol == 'Ne' and self.atomic_number == 22: return 21.991385114
        if self.symbol == 'Na' and self.atomic_number == 23: return 22.989769282
        if self.symbol == 'Mg' and self.atomic_number == 24: return 23.985041697
        if self.symbol == 'Mg' and self.atomic_number == 25: return 24.985836976
        if self.symbol == 'Mg' and self.atomic_number == 26: return 25.982592968
        if self.symbol == 'Al' and self.atomic_number == 27: return 26.98153853
        if self.symbol == 'Si' and self.atomic_number == 28: return 27.9769265347
        if self.symbol == 'Si' and self.atomic_number == 29: return 28.9764946649
        if self.symbol == 'Si' and self.atomic_number == 30: return 29.973770136
        if self.symbol == 'P' and self.atomic_number == 31: return 30.9737619984
        if self.symbol == 'S' and self.atomic_number == 32: return 31.9720711744
        if self.symbol == 'S' and self.atomic_number == 33: return 32.9714589098
        if self.symbol == 'S' and self.atomic_number == 34: return 33.967867004
        if self.symbol == 'S' and self.atomic_number == 36: return 35.96708071
        if self.symbol == 'Cl' and self.atomic_number == 35: return 34.968852682
        if self.symbol == 'Cl' and self.atomic_number == 37: return 36.965902602
        if self.symbol == 'Ar' and self.atomic_number == 36: return 35.967545105
        if self.symbol == 'Ar' and self.atomic_number == 38: return 37.96273211
        if self.symbol == 'Ar' and self.atomic_number == 40: return 39.9623831237
        if self.symbol == 'K' and self.atomic_number == 39: return 38.9637064864
        if self.symbol == 'K' and self.atomic_number == 40: return 39.963998166
        if self.symbol == 'K' and self.atomic_number == 41: return 40.9618252579
        if self.symbol == 'Ca' and self.atomic_number == 40: return 39.962590863
        if self.symbol == 'Ca' and self.atomic_number == 42: return 41.95861783
        if self.symbol == 'Ca' and self.atomic_number == 43: return 42.95876644
        if self.symbol == 'Ca' and self.atomic_number == 44: return 43.95548156
        if self.symbol == 'Ca' and self.atomic_number == 46: return 45.953689
        if self.symbol == 'Ca' and self.atomic_number == 48: return 47.95252276
        if self.symbol == 'Sc' and self.atomic_number == 45: return 44.95590828
        if self.symbol == 'Ti' and self.atomic_number == 46: return 45.95262772
        if self.symbol == 'Ti' and self.atomic_number == 47: return 46.95175879
        if self.symbol == 'Ti' and self.atomic_number == 48: return 47.94794198
        if self.symbol == 'Ti' and self.atomic_number == 49: return 48.94786568
        if self.symbol == 'Ti' and self.atomic_number == 50: return 49.94478689
        if self.symbol == 'V' and self.atomic_number == 50: return 49.94715601
        if self.symbol == 'V' and self.atomic_number == 51: return 50.94395704
        if self.symbol == 'Cr' and self.atomic_number == 50: return 49.94604183
        if self.symbol == 'Cr' and self.atomic_number == 52: return 51.94050623
        if self.symbol == 'Cr' and self.atomic_number == 53: return 52.94064815
        if self.symbol == 'Cr' and self.atomic_number == 54: return 53.93887916
        if self.symbol == 'Mn' and self.atomic_number == 55: return 54.93804391
        if self.symbol == 'Fe' and self.atomic_number == 54: return 53.93960899
        if self.symbol == 'Fe' and self.atomic_number == 56: return 55.93493633
        if self.symbol == 'Fe' and self.atomic_number == 57: return 56.93539284
        if self.symbol == 'Fe' and self.atomic_number == 58: return 57.93327443
        if self.symbol == 'Co' and self.atomic_number == 59: return 58.93319429
        if self.symbol == 'Ni' and self.atomic_number == 58: return 57.93534241
        if self.symbol == 'Ni' and self.atomic_number == 60: return 59.93078588
        if self.symbol == 'Ni' and self.atomic_number == 61: return 60.93105557
        if self.symbol == 'Ni' and self.atomic_number == 62: return 61.92834537
        if self.symbol == 'Ni' and self.atomic_number == 64: return 63.92796682
        if self.symbol == 'Cu' and self.atomic_number == 63: return 62.92959772
        if self.symbol == 'Cu' and self.atomic_number == 65: return 64.9277897
        if self.symbol == 'Zn' and self.atomic_number == 64: return 63.92914201
        if self.symbol == 'Zn' and self.atomic_number == 66: return 65.92603381
        if self.symbol == 'Zn' and self.atomic_number == 67: return 66.92712775
        if self.symbol == 'Zn' and self.atomic_number == 68: return 67.92484455
        if self.symbol == 'Zn' and self.atomic_number == 70: return 69.9253192
        if self.symbol == 'Ga' and self.atomic_number == 69: return 68.9255735
        if self.symbol == 'Ga' and self.atomic_number == 71: return 70.92470258
        if self.symbol == 'Ge' and self.atomic_number == 70: return 69.92424875
        if self.symbol == 'Ge' and self.atomic_number == 72: return 71.922075826
        if self.symbol == 'Ge' and self.atomic_number == 73: return 72.923458956
        if self.symbol == 'Ge' and self.atomic_number == 74: return 73.921177761
        if self.symbol == 'Ge' and self.atomic_number == 76: return 75.921402726
        if self.symbol == 'As' and self.atomic_number == 75: return 74.92159457
        if self.symbol == 'Se' and self.atomic_number == 74: return 73.922475934
        if self.symbol == 'Se' and self.atomic_number == 76: return 75.919213704
        if self.symbol == 'Se' and self.atomic_number == 77: return 76.919914154
        if self.symbol == 'Se' and self.atomic_number == 78: return 77.91730928
        if self.symbol == 'Se' and self.atomic_number == 80: return 79.9165218
        if self.symbol == 'Se' and self.atomic_number == 82: return 81.9166995
        if self.symbol == 'Br' and self.atomic_number == 79: return 78.9183376
        if self.symbol == 'Br' and self.atomic_number == 81: return 80.9162897
        if self.symbol == 'Kr' and self.atomic_number == 78: return 77.92036494
        if self.symbol == 'Kr' and self.atomic_number == 80: return 79.91637808
        if self.symbol == 'Kr' and self.atomic_number == 82: return 81.91348273
        if self.symbol == 'Kr' and self.atomic_number == 83: return 82.91412716
        if self.symbol == 'Kr' and self.atomic_number == 84: return 83.9114977282
        if self.symbol == 'Kr' and self.atomic_number == 86: return 85.9106106269
        if self.symbol == 'Rb' and self.atomic_number == 85: return 84.9117897379
        if self.symbol == 'Rb' and self.atomic_number == 87: return 86.909180531
        if self.symbol == 'Sr' and self.atomic_number == 84: return 83.9134191
        if self.symbol == 'Sr' and self.atomic_number == 86: return 85.9092606
        if self.symbol == 'Sr' and self.atomic_number == 87: return 86.9088775
        if self.symbol == 'Sr' and self.atomic_number == 88: return 87.9056125
        if self.symbol == 'Y' and self.atomic_number == 89: return 88.9058403
        if self.symbol == 'Zr' and self.atomic_number == 90: return 89.9046977
        if self.symbol == 'Zr' and self.atomic_number == 91: return 90.9056396
        if self.symbol == 'Zr' and self.atomic_number == 92: return 91.9050347
        if self.symbol == 'Zr' and self.atomic_number == 94: return 93.9063108
        if self.symbol == 'Zr' and self.atomic_number == 96: return 95.9082714
        if self.symbol == 'Nb' and self.atomic_number == 93: return 92.906373
        if self.symbol == 'Mo' and self.atomic_number == 92: return 91.90680796
        if self.symbol == 'Mo' and self.atomic_number == 94: return 93.9050849
        if self.symbol == 'Mo' and self.atomic_number == 95: return 94.90583877
        if self.symbol == 'Mo' and self.atomic_number == 96: return 95.90467612
        if self.symbol == 'Mo' and self.atomic_number == 97: return 96.90601812
        if self.symbol == 'Mo' and self.atomic_number == 98: return 97.90540482
        if self.symbol == 'Mo' and self.atomic_number == 100: return 99.9074718
        if self.symbol == 'Ru' and self.atomic_number == 96: return 95.90759025
        if self.symbol == 'Ru' and self.atomic_number == 98: return 97.9052868
        if self.symbol == 'Ru' and self.atomic_number == 99: return 98.9059341
        if self.symbol == 'Ru' and self.atomic_number == 100: return 99.9042143
        if self.symbol == 'Ru' and self.atomic_number == 101: return 100.9055769
        if self.symbol == 'Ru' and self.atomic_number == 102: return 101.9043441
        if self.symbol == 'Ru' and self.atomic_number == 104: return 103.9054275
        if self.symbol == 'Rh' and self.atomic_number == 103: return 102.905498
        if self.symbol == 'Pd' and self.atomic_number == 102: return 101.9056022
        if self.symbol == 'Pd' and self.atomic_number == 104: return 103.9040305
        if self.symbol == 'Pd' and self.atomic_number == 105: return 104.9050796
        if self.symbol == 'Pd' and self.atomic_number == 106: return 105.9034804
        if self.symbol == 'Pd' and self.atomic_number == 108: return 107.9038916
        if self.symbol == 'Pd' and self.atomic_number == 110: return 109.9051722
        if self.symbol == 'Ag' and self.atomic_number == 107: return 106.9050916
        if self.symbol == 'Ag' and self.atomic_number == 109: return 108.9047553
        if self.symbol == 'Cd' and self.atomic_number == 106: return 105.9064599
        if self.symbol == 'Cd' and self.atomic_number == 108: return 107.9041834
        if self.symbol == 'Cd' and self.atomic_number == 110: return 109.90300661
        if self.symbol == 'Cd' and self.atomic_number == 111: return 110.90418287
        if self.symbol == 'Cd' and self.atomic_number == 112: return 111.90276287
        if self.symbol == 'Cd' and self.atomic_number == 113: return 112.90440813
        if self.symbol == 'Cd' and self.atomic_number == 114: return 113.90336509
        if self.symbol == 'Cd' and self.atomic_number == 116: return 115.90476315
        if self.symbol == 'In' and self.atomic_number == 113: return 112.90406184
        if self.symbol == 'In' and self.atomic_number == 115: return 114.903878776
        if self.symbol == 'Sn' and self.atomic_number == 112: return 111.90482387
        if self.symbol == 'Sn' and self.atomic_number == 114: return 113.9027827
        if self.symbol == 'Sn' and self.atomic_number == 115: return 114.903344699
        if self.symbol == 'Sn' and self.atomic_number == 116: return 115.9017428
        if self.symbol == 'Sn' and self.atomic_number == 117: return 116.90295398
        if self.symbol == 'Sn' and self.atomic_number == 118: return 117.90160657
        if self.symbol == 'Sn' and self.atomic_number == 119: return 118.90331117
        if self.symbol == 'Sn' and self.atomic_number == 120: return 119.90220163
        if self.symbol == 'Sn' and self.atomic_number == 122: return 121.9034438
        if self.symbol == 'Sn' and self.atomic_number == 124: return 123.9052766
        if self.symbol == 'Sb' and self.atomic_number == 121: return 120.903812
        if self.symbol == 'Sb' and self.atomic_number == 123: return 122.9042132
        if self.symbol == 'Te' and self.atomic_number == 120: return 119.9040593
        if self.symbol == 'Te' and self.atomic_number == 122: return 121.9030435
        if self.symbol == 'Te' and self.atomic_number == 123: return 122.9042698
        if self.symbol == 'Te' and self.atomic_number == 124: return 123.9028171
        if self.symbol == 'Te' and self.atomic_number == 125: return 124.9044299
        if self.symbol == 'Te' and self.atomic_number == 126: return 125.9033109
        if self.symbol == 'Te' and self.atomic_number == 128: return 127.90446128
        if self.symbol == 'Te' and self.atomic_number == 130: return 129.906222748
        if self.symbol == 'I' and self.atomic_number == 127: return 126.9044719
        if self.symbol == 'Xe' and self.atomic_number == 124: return 123.905892
        if self.symbol == 'Xe' and self.atomic_number == 126: return 125.9042983
        if self.symbol == 'Xe' and self.atomic_number == 128: return 127.903531
        if self.symbol == 'Xe' and self.atomic_number == 129: return 128.9047808611
        if self.symbol == 'Xe' and self.atomic_number == 130: return 129.903509349
        if self.symbol == 'Xe' and self.atomic_number == 131: return 130.90508406
        if self.symbol == 'Xe' and self.atomic_number == 132: return 131.9041550856
        if self.symbol == 'Xe' and self.atomic_number == 134: return 133.90539466
        if self.symbol == 'Xe' and self.atomic_number == 136: return 135.907214484
        if self.symbol == 'Cs' and self.atomic_number == 133: return 132.905451961
        if self.symbol == 'Ba' and self.atomic_number == 130: return 129.9063207
        if self.symbol == 'Ba' and self.atomic_number == 132: return 131.9050611
        if self.symbol == 'Ba' and self.atomic_number == 134: return 133.90450818
        if self.symbol == 'Ba' and self.atomic_number == 135: return 134.90568838
        if self.symbol == 'Ba' and self.atomic_number == 136: return 135.90457573
        if self.symbol == 'Ba' and self.atomic_number == 137: return 136.90582714
        if self.symbol == 'Ba' and self.atomic_number == 138: return 137.905247
        if self.symbol == 'La' and self.atomic_number == 138: return 137.9071149
        if self.symbol == 'La' and self.atomic_number == 139: return 138.9063563
        if self.symbol == 'Ce' and self.atomic_number == 136: return 135.90712921
        if self.symbol == 'Ce' and self.atomic_number == 138: return 137.905991
        if self.symbol == 'Ce' and self.atomic_number == 140: return 139.9054431
        if self.symbol == 'Ce' and self.atomic_number == 142: return 141.9092504
        if self.symbol == 'Pr' and self.atomic_number == 141: return 140.9076576
        if self.symbol == 'Nd' and self.atomic_number == 142: return 141.907729
        if self.symbol == 'Nd' and self.atomic_number == 143: return 142.90982
        if self.symbol == 'Nd' and self.atomic_number == 144: return 143.910093
        if self.symbol == 'Nd' and self.atomic_number == 145: return 144.9125793
        if self.symbol == 'Nd' and self.atomic_number == 146: return 145.9131226
        if self.symbol == 'Nd' and self.atomic_number == 148: return 147.9168993
        if self.symbol == 'Nd' and self.atomic_number == 150: return 149.9209022
        if self.symbol == 'Sm' and self.atomic_number == 144: return 143.9120065
        if self.symbol == 'Sm' and self.atomic_number == 147: return 146.9149044
        if self.symbol == 'Sm' and self.atomic_number == 148: return 147.9148292
        if self.symbol == 'Sm' and self.atomic_number == 149: return 148.9171921
        if self.symbol == 'Sm' and self.atomic_number == 150: return 149.9172829
        if self.symbol == 'Sm' and self.atomic_number == 152: return 151.9197397
        if self.symbol == 'Sm' and self.atomic_number == 154: return 153.9222169
        if self.symbol == 'Eu' and self.atomic_number == 151: return 150.9198578
        if self.symbol == 'Eu' and self.atomic_number == 153: return 152.921238
        if self.symbol == 'Gd' and self.atomic_number == 152: return 151.9197995
        if self.symbol == 'Gd' and self.atomic_number == 154: return 153.9208741
        if self.symbol == 'Gd' and self.atomic_number == 155: return 154.9226305
        if self.symbol == 'Gd' and self.atomic_number == 156: return 155.9221312
        if self.symbol == 'Gd' and self.atomic_number == 157: return 156.9239686
        if self.symbol == 'Gd' and self.atomic_number == 158: return 157.9241123
        if self.symbol == 'Gd' and self.atomic_number == 160: return 159.9270624
        if self.symbol == 'Tb' and self.atomic_number == 159: return 158.9253547
        if self.symbol == 'Dy' and self.atomic_number == 156: return 155.9242847
        if self.symbol == 'Dy' and self.atomic_number == 158: return 157.9244159
        if self.symbol == 'Dy' and self.atomic_number == 160: return 159.9252046
        if self.symbol == 'Dy' and self.atomic_number == 161: return 160.9269405
        if self.symbol == 'Dy' and self.atomic_number == 162: return 161.9268056
        if self.symbol == 'Dy' and self.atomic_number == 163: return 162.9287383
        if self.symbol == 'Dy' and self.atomic_number == 164: return 163.9291819
        if self.symbol == 'Ho' and self.atomic_number == 165: return 164.9303288
        if self.symbol == 'Er' and self.atomic_number == 162: return 161.9287884
        if self.symbol == 'Er' and self.atomic_number == 164: return 163.9292088
        if self.symbol == 'Er' and self.atomic_number == 166: return 165.9302995
        if self.symbol == 'Er' and self.atomic_number == 167: return 166.9320546
        if self.symbol == 'Er' and self.atomic_number == 168: return 167.9323767
        if self.symbol == 'Er' and self.atomic_number == 170: return 169.9354702
        if self.symbol == 'Tm' and self.atomic_number == 169: return 168.9342179
        if self.symbol == 'Yb' and self.atomic_number == 168: return 167.9338896
        if self.symbol == 'Yb' and self.atomic_number == 170: return 169.9347664
        if self.symbol == 'Yb' and self.atomic_number == 171: return 170.9363302
        if self.symbol == 'Yb' and self.atomic_number == 172: return 171.9363859
        if self.symbol == 'Yb' and self.atomic_number == 173: return 172.9382151
        if self.symbol == 'Yb' and self.atomic_number == 174: return 173.9388664
        if self.symbol == 'Yb' and self.atomic_number == 176: return 175.9425764
        if self.symbol == 'Lu' and self.atomic_number == 175: return 174.9407752
        if self.symbol == 'Lu' and self.atomic_number == 176: return 175.9426897
        if self.symbol == 'Hf' and self.atomic_number == 174: return 173.9400461
        if self.symbol == 'Hf' and self.atomic_number == 176: return 175.9414076
        if self.symbol == 'Hf' and self.atomic_number == 177: return 176.9432277
        if self.symbol == 'Hf' and self.atomic_number == 178: return 177.9437058
        if self.symbol == 'Hf' and self.atomic_number == 179: return 178.9458232
        if self.symbol == 'Hf' and self.atomic_number == 180: return 179.946557
        if self.symbol == 'Ta' and self.atomic_number == 180: return 179.9474648
        if self.symbol == 'Ta' and self.atomic_number == 181: return 180.9479958
        if self.symbol == 'W' and self.atomic_number == 180: return 179.9467108
        if self.symbol == 'W' and self.atomic_number == 182: return 181.94820394
        if self.symbol == 'W' and self.atomic_number == 183: return 182.95022275
        if self.symbol == 'W' and self.atomic_number == 184: return 183.95093092
        if self.symbol == 'W' and self.atomic_number == 186: return 185.9543628
        if self.symbol == 'Re' and self.atomic_number == 185: return 184.9529545
        if self.symbol == 'Re' and self.atomic_number == 187: return 186.9557501
        if self.symbol == 'Os' and self.atomic_number == 184: return 183.9524885
        if self.symbol == 'Os' and self.atomic_number == 186: return 185.953835
        if self.symbol == 'Os' and self.atomic_number == 187: return 186.9557474
        if self.symbol == 'Os' and self.atomic_number == 188: return 187.9558352
        if self.symbol == 'Os' and self.atomic_number == 189: return 188.9581442
        if self.symbol == 'Os' and self.atomic_number == 190: return 189.9584437
        if self.symbol == 'Os' and self.atomic_number == 192: return 191.961477
        if self.symbol == 'Ir' and self.atomic_number == 191: return 190.9605893
        if self.symbol == 'Ir' and self.atomic_number == 193: return 192.9629216
        if self.symbol == 'Pt' and self.atomic_number == 190: return 189.9599297
        if self.symbol == 'Pt' and self.atomic_number == 192: return 191.9610387
        if self.symbol == 'Pt' and self.atomic_number == 194: return 193.9626809
        if self.symbol == 'Pt' and self.atomic_number == 195: return 194.9647917
        if self.symbol == 'Pt' and self.atomic_number == 196: return 195.96495209
        if self.symbol == 'Pt' and self.atomic_number == 198: return 197.9678949
        if self.symbol == 'Au' and self.atomic_number == 197: return 196.96656879
        if self.symbol == 'Hg' and self.atomic_number == 196: return 195.9658326
        if self.symbol == 'Hg' and self.atomic_number == 198: return 197.9667686
        if self.symbol == 'Hg' and self.atomic_number == 199: return 198.96828064
        if self.symbol == 'Hg' and self.atomic_number == 200: return 199.96832659
        if self.symbol == 'Hg' and self.atomic_number == 201: return 200.97030284
        if self.symbol == 'Hg' and self.atomic_number == 202: return 201.9706434
        if self.symbol == 'Hg' and self.atomic_number == 204: return 203.97349398
        if self.symbol == 'Tl' and self.atomic_number == 203: return 202.9723446
        if self.symbol == 'Tl' and self.atomic_number == 205: return 204.9744278
        if self.symbol == 'Pb' and self.atomic_number == 204: return 203.973044
        if self.symbol == 'Pb' and self.atomic_number == 206: return 205.9744657
        if self.symbol == 'Pb' and self.atomic_number == 207: return 206.9758973
        if self.symbol == 'Pb' and self.atomic_number == 208: return 207.9766525
        if self.symbol == 'Bi' and self.atomic_number == 209: return 208.9803991
        if self.symbol == 'Th' and self.atomic_number == 232: return 232.0380558
        if self.symbol == 'Pa' and self.atomic_number == 231: return 231.0358842
        if self.symbol == 'U' and self.atomic_number == 234: return 234.0409523
        if self.symbol == 'U' and self.atomic_number == 235: return 235.0439301
        if self.symbol == 'U' and self.atomic_number == 238:
            return 238.0507884
        else:
            print(self.symbol, ' not found no mass to return ')

    @property
    def description(self):
        print('isotope ', self.symbol, self.atomic_number, self.abundance, self.mass_amu)


class Element:
    def __init__(self, symbol, enriched_isotopes='Natural'):
        self.symbol = symbol
        try:
            self.enriched_isotopes = enriched_isotopes
        except:
            self.enriched_isotopes = 'Natural'

    @property
    def natural_isotopes_in_elements(self):
        if self.symbol == 'Sn': return [Isotope(self.symbol, 112), Isotope(self.symbol, 114), Isotope(self.symbol, 115),
                                        Isotope(self.symbol, 116), Isotope(self.symbol, 117),
                                        Isotope(self.symbol, 118), Isotope(self.symbol, 119), Isotope(self.symbol, 120),
                                        Isotope(self.symbol, 122), Isotope(self.symbol, 124)]
        if self.symbol == 'Xe': return [Isotope(self.symbol, 124), Isotope(self.symbol, 126), Isotope(self.symbol, 128),
                                        Isotope(self.symbol, 129), Isotope(self.symbol, 130),
                                        Isotope(self.symbol, 131), Isotope(self.symbol, 132), Isotope(self.symbol, 134),
                                        Isotope(self.symbol, 136)]
        if self.symbol == 'Cd': return [Isotope(self.symbol, 106), Isotope(self.symbol, 108), Isotope(self.symbol, 110),
                                        Isotope(self.symbol, 111), Isotope(self.symbol, 112), Isotope(self.symbol, 113),
                                        Isotope(self.symbol, 114), Isotope(self.symbol, 116)]
        if self.symbol == 'Te': return [Isotope(self.symbol, 120), Isotope(self.symbol, 122), Isotope(self.symbol, 123),
                                        Isotope(self.symbol, 124), Isotope(self.symbol, 125), Isotope(self.symbol, 126),
                                        Isotope(self.symbol, 128), Isotope(self.symbol, 130)]
        if self.symbol == 'Ba': return [Isotope(self.symbol, 130), Isotope(self.symbol, 132), Isotope(self.symbol, 134),
                                        Isotope(self.symbol, 135), Isotope(self.symbol, 136), Isotope(self.symbol, 137),
                                        Isotope(self.symbol, 138)]
        if self.symbol == 'Dy': return [Isotope(self.symbol, 156), Isotope(self.symbol, 158), Isotope(self.symbol, 160),
                                        Isotope(self.symbol, 161), Isotope(self.symbol, 162), Isotope(self.symbol, 163),
                                        Isotope(self.symbol, 164)]
        if self.symbol == 'Gd': return [Isotope(self.symbol, 152), Isotope(self.symbol, 154), Isotope(self.symbol, 155),
                                        Isotope(self.symbol, 156), Isotope(self.symbol, 157), Isotope(self.symbol, 158),
                                        Isotope(self.symbol, 160)]
        if self.symbol == 'Hg': return [Isotope(self.symbol, 196), Isotope(self.symbol, 198), Isotope(self.symbol, 199),
                                        Isotope(self.symbol, 200), Isotope(self.symbol, 201), Isotope(self.symbol, 202),
                                        Isotope(self.symbol, 204)]
        if self.symbol == 'Mo': return [Isotope(self.symbol, 92), Isotope(self.symbol, 94), Isotope(self.symbol, 95),
                                        Isotope(self.symbol, 96), Isotope(self.symbol, 97), Isotope(self.symbol, 98),
                                        Isotope(self.symbol, 100)]
        if self.symbol == 'Nd': return [Isotope(self.symbol, 142), Isotope(self.symbol, 143), Isotope(self.symbol, 144),
                                        Isotope(self.symbol, 145), Isotope(self.symbol, 146), Isotope(self.symbol, 148),
                                        Isotope(self.symbol, 150)]
        if self.symbol == 'Os': return [Isotope(self.symbol, 184), Isotope(self.symbol, 186), Isotope(self.symbol, 187),
                                        Isotope(self.symbol, 188), Isotope(self.symbol, 189), Isotope(self.symbol, 190),
                                        Isotope(self.symbol, 192)]
        if self.symbol == 'Ru': return [Isotope(self.symbol, 96), Isotope(self.symbol, 98), Isotope(self.symbol, 99),
                                        Isotope(self.symbol, 100), Isotope(self.symbol, 101),
                                        Isotope(self.symbol, 102), Isotope(self.symbol, 104)]
        if self.symbol == 'Sm': return [Isotope(self.symbol, 144), Isotope(self.symbol, 147), Isotope(self.symbol, 148),
                                        Isotope(self.symbol, 149), Isotope(self.symbol, 150),
                                        Isotope(self.symbol, 152), Isotope(self.symbol, 154)]
        if self.symbol == 'Yb': return [Isotope(self.symbol, 168), Isotope(self.symbol, 170), Isotope(self.symbol, 171),
                                        Isotope(self.symbol, 172), Isotope(self.symbol, 173),
                                        Isotope(self.symbol, 174), Isotope(self.symbol, 176)]
        if self.symbol == 'Ca': return [Isotope(self.symbol, 40), Isotope(self.symbol, 42), Isotope(self.symbol, 43),
                                        Isotope(self.symbol, 44), Isotope(self.symbol, 46), Isotope(self.symbol, 48)]
        if self.symbol == 'Er': return [Isotope(self.symbol, 162), Isotope(self.symbol, 164), Isotope(self.symbol, 166),
                                        Isotope(self.symbol, 167), Isotope(self.symbol, 168), Isotope(self.symbol, 170)]
        if self.symbol == 'Hf': return [Isotope(self.symbol, 174), Isotope(self.symbol, 176), Isotope(self.symbol, 177),
                                        Isotope(self.symbol, 178), Isotope(self.symbol, 179), Isotope(self.symbol, 180)]
        if self.symbol == 'Kr': return [Isotope(self.symbol, 78), Isotope(self.symbol, 80), Isotope(self.symbol, 82),
                                        Isotope(self.symbol, 83), Isotope(self.symbol, 84), Isotope(self.symbol, 86)]
        if self.symbol == 'Pd': return [Isotope(self.symbol, 102), Isotope(self.symbol, 104), Isotope(self.symbol, 105),
                                        Isotope(self.symbol, 106), Isotope(self.symbol, 108), Isotope(self.symbol, 110)]
        if self.symbol == 'Pt': return [Isotope(self.symbol, 190), Isotope(self.symbol, 192), Isotope(self.symbol, 194),
                                        Isotope(self.symbol, 195), Isotope(self.symbol, 196), Isotope(self.symbol, 198)]
        if self.symbol == 'Se': return [Isotope(self.symbol, 74), Isotope(self.symbol, 76), Isotope(self.symbol, 77),
                                        Isotope(self.symbol, 78), Isotope(self.symbol, 80), Isotope(self.symbol, 82)]
        if self.symbol == 'Ge': return [Isotope(self.symbol, 70), Isotope(self.symbol, 72), Isotope(self.symbol, 73),
                                        Isotope(self.symbol, 74), Isotope(self.symbol, 76)]
        if self.symbol == 'Ni': return [Isotope(self.symbol, 58), Isotope(self.symbol, 60), Isotope(self.symbol, 61),
                                        Isotope(self.symbol, 62), Isotope(self.symbol, 64)]
        if self.symbol == 'Ti': return [Isotope(self.symbol, 46), Isotope(self.symbol, 47), Isotope(self.symbol, 48),
                                        Isotope(self.symbol, 49), Isotope(self.symbol, 50)]
        if self.symbol == 'W': return [Isotope(self.symbol, 180), Isotope(self.symbol, 182), Isotope(self.symbol, 183),
                                       Isotope(self.symbol, 184), Isotope(self.symbol, 186)]
        if self.symbol == 'Zn': return [Isotope(self.symbol, 64), Isotope(self.symbol, 66), Isotope(self.symbol, 67),
                                        Isotope(self.symbol, 68), Isotope(self.symbol, 70)]
        if self.symbol == 'Zr': return [Isotope(self.symbol, 90), Isotope(self.symbol, 91), Isotope(self.symbol, 92),
                                        Isotope(self.symbol, 94), Isotope(self.symbol, 96)]
        if self.symbol == 'Ce': return [Isotope(self.symbol, 136), Isotope(self.symbol, 138), Isotope(self.symbol, 140),
                                        Isotope(self.symbol, 142)]
        if self.symbol == 'Cr': return [Isotope(self.symbol, 50), Isotope(self.symbol, 52), Isotope(self.symbol, 53),
                                        Isotope(self.symbol, 54)]
        if self.symbol == 'Fe': return [Isotope(self.symbol, 54), Isotope(self.symbol, 56), Isotope(self.symbol, 57),
                                        Isotope(self.symbol, 58)]
        if self.symbol == 'Pb': return [Isotope(self.symbol, 204), Isotope(self.symbol, 206), Isotope(self.symbol, 207),
                                        Isotope(self.symbol, 208)]
        if self.symbol == 'S': return [Isotope(self.symbol, 32), Isotope(self.symbol, 33), Isotope(self.symbol, 34),
                                       Isotope(self.symbol, 36)]
        if self.symbol == 'Sr': return [Isotope(self.symbol, 84), Isotope(self.symbol, 86), Isotope(self.symbol, 87),
                                        Isotope(self.symbol, 88)]
        if self.symbol == 'Ar': return [Isotope(self.symbol, 36), Isotope(self.symbol, 38), Isotope(self.symbol, 40)]
        if self.symbol == 'C': return [Isotope(self.symbol, 12), Isotope(self.symbol, 13), Isotope(self.symbol, 14)]
        if self.symbol == 'K': return [Isotope(self.symbol, 39), Isotope(self.symbol, 40), Isotope(self.symbol, 41)]
        if self.symbol == 'Mg': return [Isotope(self.symbol, 24), Isotope(self.symbol, 25), Isotope(self.symbol, 26)]
        if self.symbol == 'Ne': return [Isotope(self.symbol, 20), Isotope(self.symbol, 21), Isotope(self.symbol, 22)]
        if self.symbol == 'Si': return [Isotope(self.symbol, 28), Isotope(self.symbol, 29), Isotope(self.symbol, 30)]
        if self.symbol == 'U': return [Isotope(self.symbol, 234), Isotope(self.symbol, 235), Isotope(self.symbol, 238)]
        if self.symbol == 'Ag': return [Isotope(self.symbol, 107), Isotope(self.symbol, 109)]
        if self.symbol == 'B': return [Isotope(self.symbol, 10), Isotope(self.symbol, 11)]
        if self.symbol == 'Br': return [Isotope(self.symbol, 79), Isotope(self.symbol, 81)]
        if self.symbol == 'Cl': return [Isotope(self.symbol, 35), Isotope(self.symbol, 37)]
        if self.symbol == 'Cu': return [Isotope(self.symbol, 63), Isotope(self.symbol, 65)]
        if self.symbol == 'Eu': return [Isotope(self.symbol, 151), Isotope(self.symbol, 153)]
        if self.symbol == 'Ga': return [Isotope(self.symbol, 69), Isotope(self.symbol, 71)]
        if self.symbol == 'H':  return [Isotope(self.symbol, 1), Isotope(self.symbol, 2)]
        if self.symbol == 'He': return [Isotope(self.symbol, 3), Isotope(self.symbol, 4)]
        if self.symbol == 'In': return [Isotope(self.symbol, 113), Isotope(self.symbol, 115)]
        if self.symbol == 'Ir': return [Isotope(self.symbol, 191), Isotope(self.symbol, 193)]
        if self.symbol == 'La': return [Isotope(self.symbol, 138), Isotope(self.symbol, 139)]
        if self.symbol == 'Li': return [Isotope(self.symbol, 6), Isotope(self.symbol, 7)]
        if self.symbol == 'Lu': return [Isotope(self.symbol, 175), Isotope(self.symbol, 176)]
        if self.symbol == 'N':  return [Isotope(self.symbol, 14), Isotope(self.symbol, 15)]
        if self.symbol == 'Rb': return [Isotope(self.symbol, 85), Isotope(self.symbol, 87)]
        if self.symbol == 'Re': return [Isotope(self.symbol, 185), Isotope(self.symbol, 187)]
        if self.symbol == 'Sb': return [Isotope(self.symbol, 121), Isotope(self.symbol, 123)]
        if self.symbol == 'Ta': return [Isotope(self.symbol, 180), Isotope(self.symbol, 181)]
        if self.symbol == 'Tl': return [Isotope(self.symbol, 203), Isotope(self.symbol, 205)]
        if self.symbol == 'V':  return [Isotope(self.symbol, 50), Isotope(self.symbol, 51)]
        if self.symbol == 'Be': return [Isotope(self.symbol, 9)]
        if self.symbol == 'O':  return [Isotope(self.symbol, 16)]
        if self.symbol == 'F':  return [Isotope(self.symbol, 19)]
        if self.symbol == 'Na': return [Isotope(self.symbol, 23)]
        if self.symbol == 'Al': return [Isotope(self.symbol, 27)]
        if self.symbol == 'P':  return [Isotope(self.symbol, 31)]
        if self.symbol == 'Sc': return [Isotope(self.symbol, 45)]
        if self.symbol == 'Mn': return [Isotope(self.symbol, 55)]
        if self.symbol == 'Co': return [Isotope(self.symbol, 59)]
        if self.symbol == 'As': return [Isotope(self.symbol, 75)]
        if self.symbol == 'Y':  return [Isotope(self.symbol, 89)]
        if self.symbol == 'Nb': return [Isotope(self.symbol, 93)]
        if self.symbol == 'Rh': return [Isotope(self.symbol, 103)]
        if self.symbol == 'I':  return [Isotope(self.symbol, 127)]
        if self.symbol == 'Cs': return [Isotope(self.symbol, 133)]
        if self.symbol == 'Pr': return [Isotope(self.symbol, 141)]
        if self.symbol == 'Tb': return [Isotope(self.symbol, 159)]
        if self.symbol == 'Ho': return [Isotope(self.symbol, 165)]
        if self.symbol == 'Tm': return [Isotope(self.symbol, 169)]
        if self.symbol == 'Au': return [Isotope(self.symbol, 197)]
        if self.symbol == 'Bi': return [Isotope(self.symbol, 209)]
        if self.symbol == 'Th': return [Isotope(self.symbol, 232)]
        if self.symbol == 'Pa':
            return [Isotope(self.symbol, 231)]

        else:
            print('natural composition of isotope not found', self.symbol)
            sys.exit()

    @property
    def molar_mass_g(self):
        element_mass = 0
        for isotope in self.isotopes:
            # isotope.print_details
            # print(isotope.abundance, isotope.mass_amu)
            element_mass = element_mass + isotope.abundance * isotope.mass_amu
        return element_mass

    @property
    def protons(self):
        if self.symbol == 'H': return 1
        if self.symbol == 'He': return 2
        if self.symbol == 'Li': return 3
        if self.symbol == 'Be': return 4
        if self.symbol == 'B': return 5
        if self.symbol == 'C': return 6
        if self.symbol == 'N': return 7
        if self.symbol == 'O': return 8
        if self.symbol == 'F': return 9
        if self.symbol == 'Ne': return 10
        if self.symbol == 'Na': return 11
        if self.symbol == 'Mg': return 12
        if self.symbol == 'Al': return 13
        if self.symbol == 'Si': return 14
        if self.symbol == 'P': return 15
        if self.symbol == 'S': return 16
        if self.symbol == 'Cl': return 17
        if self.symbol == 'Ar': return 18
        if self.symbol == 'K': return 19
        if self.symbol == 'Ca': return 20
        if self.symbol == 'Sc': return 21
        if self.symbol == 'Ti': return 22
        if self.symbol == 'V': return 23
        if self.symbol == 'Cr': return 24
        if self.symbol == 'Mn': return 25
        if self.symbol == 'Fe': return 26
        if self.symbol == 'Co': return 27
        if self.symbol == 'Ni': return 28
        if self.symbol == 'Cu': return 29
        if self.symbol == 'Zn': return 30
        if self.symbol == 'Ga': return 31
        if self.symbol == 'Ge': return 32
        if self.symbol == 'As': return 33
        if self.symbol == 'Se': return 34
        if self.symbol == 'Br': return 35
        if self.symbol == 'Kr': return 36
        if self.symbol == 'Rb': return 37
        if self.symbol == 'Sr': return 38
        if self.symbol == 'Y': return 39
        if self.symbol == 'Zr': return 40
        if self.symbol == 'Nb': return 41
        if self.symbol == 'Mo': return 42
        if self.symbol == 'Tc': return 43
        if self.symbol == 'Ru': return 44
        if self.symbol == 'Rh': return 45
        if self.symbol == 'Pd': return 46
        if self.symbol == 'Ag': return 47
        if self.symbol == 'Cd': return 48
        if self.symbol == 'In': return 49
        if self.symbol == 'Sn': return 50
        if self.symbol == 'Sb': return 51
        if self.symbol == 'Te': return 52
        if self.symbol == 'I': return 53
        if self.symbol == 'Xe': return 54
        if self.symbol == 'Cs': return 55
        if self.symbol == 'Ba': return 56
        if self.symbol == 'La': return 57
        if self.symbol == 'Ce': return 58
        if self.symbol == 'Pr': return 59
        if self.symbol == 'Nd': return 60
        if self.symbol == 'Pm': return 61
        if self.symbol == 'Sm': return 62
        if self.symbol == 'Eu': return 63
        if self.symbol == 'Gd': return 64
        if self.symbol == 'Tb': return 65
        if self.symbol == 'Dy': return 66
        if self.symbol == 'Ho': return 67
        if self.symbol == 'Er': return 68
        if self.symbol == 'Tm': return 69
        if self.symbol == 'Yb': return 70
        if self.symbol == 'Lu': return 71
        if self.symbol == 'Hf': return 72
        if self.symbol == 'Ta': return 73
        if self.symbol == 'W': return 74
        if self.symbol == 'Re': return 75
        if self.symbol == 'Os': return 76
        if self.symbol == 'Ir': return 77
        if self.symbol == 'Pt': return 78
        if self.symbol == 'Au': return 79
        if self.symbol == 'Hg': return 80
        if self.symbol == 'Tl': return 81
        if self.symbol == 'Pb': return 82
        if self.symbol == 'Bi': return 83
        if self.symbol == 'Po': return 84
        if self.symbol == 'At': return 85
        if self.symbol == 'Rn': return 86
        if self.symbol == 'Fr': return 87
        if self.symbol == 'Ra': return 88
        if self.symbol == 'Ac': return 89
        if self.symbol == 'Th': return 90
        if self.symbol == 'Pa': return 91
        if self.symbol == 'U': return 92
        if self.symbol == 'Np': return 93
        if self.symbol == 'Pu': return 94
        if self.symbol == 'Am': return 95
        if self.symbol == 'Cm': return 96
        if self.symbol == 'Bk': return 97
        if self.symbol == 'Cf': return 98
        if self.symbol == 'Es': return 99
        if self.symbol == 'Fm': return 100
        if self.symbol == 'Md': return 101
        if self.symbol == 'No': return 102
        if self.symbol == 'Lr': return 103
        if self.symbol == 'Rf': return 104
        if self.symbol == 'Db': return 105
        if self.symbol == 'Sg': return 106
        if self.symbol == 'Bh': return 107
        if self.symbol == 'Hs': return 108
        if self.symbol == 'Mt': return 109
        if self.symbol == 'Ds': return 110
        if self.symbol == 'Rg': return 111
        if self.symbol == 'Cn': return 112
        if self.symbol == 'Nh': return 113
        if self.symbol == 'Fl': return 114
        if self.symbol == 'Mc': return 115
        if self.symbol == 'Lv': return 116
        if self.symbol == 'Ts': return 117
        if self.symbol == 'Og': return 118
        return ('proton number for ' + self.symbol + ' not found')

    @property
    def isotopes(self):
        isotopes_to_make = []
        # print('enriched isotopes = ',self.enriched_isotopes)
        if self.enriched_isotopes!='Natural':
            # print('enriched isotope detected',self.symbol)
            # check that ratios add up to 1
            cummulative_abundance = 0.0
            for enriched_isotope in self.enriched_isotopes:
                cummulative_abundance = cummulative_abundance + enriched_isotope.abundance
                if enriched_isotope.abundance < 0 or enriched_isotope.abundance > 1.0:
                    print('Enriched isotopes fractions must be between 0 and 1.0, you have', enriched_isotope.abundance)
                    sys.exit()
            if cummulative_abundance != 1.0:
                print('Enriched isotopes fractions must add up to 1.0, you have', cummulative_abundance)
                sys.exit()

            # check if all isotopes for element are present
            required_isotope_list = self.natural_isotopes_in_elements
            if len(self.enriched_isotopes) != len(required_isotope_list):
                print(
                'Your number of enriched isotopes does not match the number of isotopes in the natural element composition')
                print('you have ', len(self.enriched_isotopes), ' but element ', self.symbol, ' needs ',
                      len(required_isotope_list))
                sys.exit()

            # checks each of the natural isotopes is represented in the list
            for enriched_isotope, required_isotope in zip(self.enriched_isotopes, required_isotope_list):
                if enriched_isotope.symbol != required_isotope.symbol:
                    print('Enriched isotope symbol is wrong', enriched_isotope.symbol, required_isotope.symbol)
                    sys.exit()
                if enriched_isotope.atomic_number != required_isotope.atomic_number:
                    print('Enriched isotope atomic numbers are not the same as natural material, provided',
                          enriched_isotope.atomic_number, ' is not equal to required ', required_isotope.atomic_number)
                    sys.exit()

            for enriched_isotope in self.enriched_isotopes:
                isotopes_to_make.append(enriched_isotope)

        else:
            # print('no enriched isotopes found')
            isotopes_to_make = self.natural_isotopes_in_elements

        return isotopes_to_make

    @property
    def full_name(self):  # returns full element name
        if (self.symbol == 'Ac'): return 'Actinium'
        if (self.symbol == 'Al'): return 'Aluminum'
        if (self.symbol == 'Am'): return 'Americium'
        if (self.symbol == 'Sb'): return 'Antimony'
        if (self.symbol == 'Ar'): return 'Argon'
        if (self.symbol == 'As'): return 'Arsenic'
        if (self.symbol == 'At'): return 'Astatine'
        if (self.symbol == 'Ba'): return 'Barium'
        if (self.symbol == 'Bk'): return 'Berkelium'
        if (self.symbol == 'Be'): return 'Beryllium'
        if (self.symbol == 'Bi'): return 'Bismuth'
        if (self.symbol == 'Bh'): return 'Bohrium'
        if (self.symbol == 'B'):  return 'Boron'
        if (self.symbol == 'Br'): return 'Bromine'
        if (self.symbol == 'Cd'): return 'Cadmium'
        if (self.symbol == 'Ca'): return 'Calcium'
        if (self.symbol == 'Cf'): return 'Californium'
        if (self.symbol == 'C'):  return 'Carbon'
        if (self.symbol == 'Ce'): return 'Cerium'
        if (self.symbol == 'Cs'): return 'Cesium'
        if (self.symbol == 'Cl'): return 'Chlorine'
        if (self.symbol == 'Cr'): return 'Chromium'
        if (self.symbol == 'Co'): return 'Cobalt'
        if (self.symbol == 'Cu'): return 'Copper'
        if (self.symbol == 'Cn'): return 'Copernicium'
        if (self.symbol == 'Cm'): return 'Curium'
        if (self.symbol == 'Ds'): return 'Darmstadtium'
        if (self.symbol == 'Db'): return 'Dubnium'
        if (self.symbol == 'Dy'): return 'Dysprosium'
        if (self.symbol == 'Es'): return 'Einsteinium'
        if (self.symbol == 'Er'): return 'Erbium'
        if (self.symbol == 'Eu'): return 'Europium'
        if (self.symbol == 'Fm'): return 'Fermium'
        if (self.symbol == 'F'):  return 'Fluorine'
        if (self.symbol == 'Fl'): return 'Flerovium'
        if (self.symbol == 'Fr'): return 'Francium'
        if (self.symbol == 'Gd'): return 'Gadolinium'
        if (self.symbol == 'Ga'): return 'Gallium'
        if (self.symbol == 'Ge'): return 'Germanium'
        if (self.symbol == 'Au'): return 'Gold'
        if (self.symbol == 'Hf'): return 'Hafnium'
        if (self.symbol == 'Hs'): return 'Hassium'
        if (self.symbol == 'He'): return 'Helium'
        if (self.symbol == 'Ho'): return 'Holmium'
        if (self.symbol == 'H'):  return 'Hydrogen'
        if (self.symbol == 'In'): return 'Indium'
        if (self.symbol == 'I'):  return 'Iodine'
        if (self.symbol == 'Ir'): return 'Iridium'
        if (self.symbol == 'Fe'): return 'Iron'
        if (self.symbol == 'Kr'): return 'Krypton'
        if (self.symbol == 'La'): return 'Lanthanum'
        if (self.symbol == 'Lr'): return 'Lawrencium'
        if (self.symbol == 'Pb'): return 'Lead'
        if (self.symbol == 'Li'): return 'Lithium'
        if (self.symbol == 'Lu'): return 'Lutetium'
        if (self.symbol == 'Mg'): return 'Magnesium'
        if (self.symbol == 'Mn'): return 'Manganese'
        if (self.symbol == 'Mt'): return 'Meitnerium'
        if (self.symbol == 'Md'): return 'Mendelevium'
        if (self.symbol == 'Mc'): return 'Moscovium'
        if (self.symbol == 'Hg'): return 'Mercury'
        if (self.symbol == 'Mo'): return 'Molybdenum'
        if (self.symbol == 'Nd'): return 'Neodymium'
        if (self.symbol == 'Ne'): return 'Neon'
        if (self.symbol == 'Np'): return 'Neptunium'
        if (self.symbol == 'Ni'): return 'Nickel'
        if (self.symbol == 'Nb'): return 'Niobium'
        if (self.symbol == 'N'):  return 'Nitrogen'
        if (self.symbol == 'No'): return 'Nobelium'
        if (self.symbol == 'Os'): return 'Osmium'
        if (self.symbol == 'O'):  return 'Oxygen'
        if (self.symbol == 'Pd'): return 'Palladium'
        if (self.symbol == 'P'): return 'Phosphorus'
        if (self.symbol == 'Pt'): return 'Platinum'
        if (self.symbol == 'Pu'): return 'Plutonium'
        if (self.symbol == 'Po'): return 'Polonium'
        if (self.symbol == 'K'): return 'Potassium'
        if (self.symbol == 'Pr'): return 'Praseodymium'
        if (self.symbol == 'Pm'): return 'Promethium'
        if (self.symbol == 'Pa'): return 'Protactinium'
        if (self.symbol == 'Ra'): return 'Radium'
        if (self.symbol == 'Rb'): return 'Rubidium'
        if (self.symbol == 'Rn'): return 'Radon'
        if (self.symbol == 'Re'): return 'Rhenium'
        if (self.symbol == 'Rh'): return 'Rhodium'
        if (self.symbol == 'Rg'): return 'Roentgenium'
        if (self.symbol == 'Ru'): return 'Ruthenium'
        if (self.symbol == 'Rf'): return 'Rutherfordium'
        if (self.symbol == 'Sm'): return 'Samarium'
        if (self.symbol == 'Sc'): return 'Scandium'
        if (self.symbol == 'Sg'): return 'Seaborgium'
        if (self.symbol == 'Se'): return 'Selenium'
        if (self.symbol == 'Si'): return 'Silicon'
        if (self.symbol == 'Ag'): return 'Silver'
        if (self.symbol == 'Na'): return 'Sodium'
        if (self.symbol == 'Sr'): return 'Strontium'
        if (self.symbol == 'S'): return 'Sulfur'
        if (self.symbol == 'Ta'): return 'Tantalum'
        if (self.symbol == 'Tc'): return 'Technetium'
        if (self.symbol == 'Te'): return 'Tellurium'
        if (self.symbol == 'Tb'): return 'Terbium'
        if (self.symbol == 'Tl'): return 'Thallium'
        if (self.symbol == 'Th'): return 'Thorium'
        if (self.symbol == 'Tm'): return 'Thulium'
        if (self.symbol == 'Sn'): return 'Tin'
        if (self.symbol == 'Ti'): return 'Titanium'
        if (self.symbol == 'W'): return 'Tungsten'
        if (self.symbol == 'U'): return 'Uranium'
        if (self.symbol == 'V'): return 'Vanadium'
        if (self.symbol == 'Xe'): return 'Xenon'
        if (self.symbol == 'Yb'): return 'Ytterbium'
        if (self.symbol == 'Y'): return 'Yttrium'
        if (self.symbol == 'Zn'): return 'Zinc'
        if (self.symbol == 'Zr'): return 'Zirconium'
        print('element not found', element)
        sys.exit()



class Material:
    def __init__(self,name,*enriched_isotopes):
        self.name = name
        self.enriched_isotopes = enriched_isotopes
        if enriched_isotopes:
            print('enriched materials not yet implemented')
            sys.exit()

    @property
    def is_this_available(self):
        if self.name in ['Tungsten','Eurofer','Homogenous_Magnet','plasma','SS-316L-IG']:
            return True
        else: return False

    @property
    def available_comounds(self):
        return ['Tungsten','Eurofer','Homogenous_Magnet','plasma','SS-316L-IG']


    @property
    def zaids(self):
        if self.is_this_available:
            if self.name == 'Eurofer':
                list_of_zaids = [26054, 26056, 26057, 26058, 5010, 5011, 6012, 7014, 7015, 8016, 13027, 14028, 14029, 14030, 15031,16032, 16033, 16034, 16036, 22046, 22047, 22048, 22049, 22050, 23050, 23051, 24050, 24052, 24053,             24054, 25055, 27059, 28058, 28060, 28061, 28062, 28064, 29063, 29065, 41093, 42092, 42094, 42095,42096, 42097, 42098, 42100, 73181, 74182, 74183, 74184, 74186]
            if self.name == 'Homogenous_Magnet':
                list_of_zaids = [1001,6012,7014,8016,12024,12025,12026,13027,14028,14029,14030,16032,16033,16034,16036,29063,29065,41093,50112,50114,50115,50116,50117,50118,50119,50120,50122,50124,2004,5010,5011,6012,7014,8016,13027,14028,15031,16032,16033,16034,16036,19039,19040,19041,22046,22047,22048,22049,22050,23050,23051,24050,24052,24053,24054,25055,26054,26056,26057,26058,27059,28058,28060,28061,28062,28064,29063,29065,40090,40091,40092,40094,40096,41093,42092,42094,42095,42096,42097,42098,42100,50112,50114,50115,50116,50117,50118,50119,50120,50122,50124,73181,74182,74183,74184,74186,82206,82207,82208,83209,29063,29065,50112,50114,50115,50116,50117,50118,50119,50120,50122,50124]
            if self.name == 'SS-316L-IG':
                list_of_zaids = [26054,26056,26057,26058, 6012,25055,14028,14029,14030,15031,16032,16033,16034,16036,24050,24052,24053,24054,28058,28060,28061,28062,28064,42092,42094,42095,42096,42097,42098,42100, 7014, 7015, 5010, 5011,29063,29065,27059,41093,22046,22047,22048,22049,22050,73181]
            if self.name == 'plasma':
                list_of_zaids = [1002,1003]
            if self.name == 'Tungsten':
                list_of_zaids = [74182,74183,74184,74186]
            list_of_zaids_string=[]
            for item in list_of_zaids:
                list_of_zaids_string.append(str(item)) #.zfill(3)
            return list_of_zaids_string
        else:
            return self.name +' is not in the available materials please ask Jon to add it'
    @property
    def isotopes(self):
        isotope_list = []
        for zaid in self.zaids:
            protons = int(str(zaid)[:-3])
            neutrons = int(str(zaid)[len(str(zaid))-3:])
            isotope_list.append(Isotope(protons,neutrons))
        if isotope_list!=[]:
            return isotope_list
        else:
            print('material isotope composition not found, current materials available are ',self.is_this_available)

    @property
    def atom_density_per_barn_per_cm(self):
        if self.name == 'plasma':
            return 1E-20
        if self.name == 'Homogenous_Magnet':
            return 7.194E-02
        else:
            print('material not found, current materials available are ',self.is_this_available)
            print('perhaps try atom_density_per_barn_per_cm')
            sys.exit()

    @property
    def density_g_per_cm3(self):
        if self.name == 'Eurofer':
            return 7750.0/1000.0

        if self.name == 'SS-316L-IG':
            return 7930.0/1000.0

        if self.name == 'Tungsten':
            return 19298.0/1000.0

        # if self.name == 'plasma':
        #     return self.atom_density_per_barn_per_cm * 1E24 * ((Isotope('H',2).mass_amu+Isotope('H',2).mass_amu)/2.0)*1.660539040E-27*1000.0

        else:
            print('material not found, current materials available are ',self.is_this_available)
            print('perhaps try atom_density_per_barn_per_cm')
            sys.exit()

    @property
    def isotopes_mass_fractions(self):
        isotope_list = []
        if self.name == 'Eurofer':
            return [4.56563E-03, 6.91110E-02, 1.56807E-03, 2.05084E-04, 9.43123E-07, 3.45108E-06, 4.14310E-04, 1.34911E-04,4.65085E-07, 2.95487E-06, 7.02121E-06, 4.05884E-05, 1.98991E-06, 1.26804E-06, 3.05762E-06, 4.21784E-06,3.27443E-08, 1.79397E-07, 7.89886E-10, 8.49986E-08, 7.50223E-08, 7.27880E-07, 5.23259E-08, 4.90993E-08,4.651675e-8, 0.00001856018, 3.70662E-04, 6.87292E-03, 7.64630E-04, 1.86808E-04, 4.73931E-04, 4.01637E-06,5.56272E-06, 2.07132E-06, 8.85630E-08, 2.77823E-07, 6.85423E-08, 1.56104E-06, 6.74368E-07, 2.54802E-06,2.29342E-07, 1.39911E-07, 2.38263E-07, 2.47037E-07, 1.39981E-07, 3.50081E-07, 1.36919E-07, 3.14209E-05,7.59071E-05, 4.07659E-05, 8.68119E-05, 7.96842E-05]
        if self.name == 'Homogenous_Magnet':
            return [3.89340E-03,3.40560E-03,3.70800E-04,4.87080E-03,1.69197E-04,2.14200E-05,2.35834E-05,7.07400E-04,1.32800E-03,6.72000E-05,4.46000E-03,8.71457E-05,6.97680E-07,3.93822E-06,1.83600E-08,6.83585E-03,3.05684E-03,1.18439E-03,3.82953E-06,2.60566E-06,1.34231E-06,5.74035E-05,3.03204E-05,9.56198E-05,3.39131E-05,1.28625E-04,1.82791E-05,2.28587E-05,3.08888E-03,4.06294E-07,1.48738E-06,1.70203E-04,2.77382E-04,2.55621E-06,2.27302E-04,7.27891E-05,1.65004E-05,8.47338E-07,6.78371E-09,3.82922E-08,1.78519E-10,2.43807E-07,3.05877E-11,1.75950E-08,1.40904E-06,1.27070E-06,1.259086E-05,9.23990E-07,8.84708E-07,4.013075e-9,0.00000160121,3.03427E-04,5.67045E-03,6.34294E-04,1.55150E-04,5.58169E-04,1.46579E-03,2.21899E-02,5.16004E-04,7.24415E-05,1.73444E-05,2.89354E-03,1.07976E-03,5.07463E-05,1.46193E-04,4.48851E-05,2.24477E-05,9.72920E-06,2.30510E-07,5.02686E-08,7.68366E-08,7.78671E-08,1.25448E-08,1.21022E-05,6.32488E-05,3.94240E-05,6.78518E-05,7.10910E-05,4.24074E-05,1.02843E-04,4.10435E-05,3.34089E-09,2.27319E-09,1.17103E-09,5.00790E-08,2.64516E-08,8.34190E-08,2.95859E-08,1.12213E-07,1.59467E-08,1.99420E-08,5.64891E-08,2.96623E-08,1.60908E-08,3.40070E-08,3.12220E-08,1.78732E-08,1.78578E-08,4.19446E-08,7.82589E-08,3.38912E-03,1.51554E-03,2.54666E-06,1.73278E-06,8.92643E-07,3.81736E-05,2.01632E-05,6.35877E-05,2.25524E-05,8.55362E-05,1.21557E-05,1.52012E-0]
        if self.name == 'SS-316L-IG':
            return [3.29181E-03,4.98289E-02,1.13058E-03,1.47865E-04,1.19277E-04,1.73653E-03,7.86496E-04,3.85593E-05,2.45713E-05,3.85117E-05,1.41667E-05,1.09980E-07,6.02549E-07,2.65303E-09,7.46975E-04,1.38506E-02,1.54092E-03,3.76464E-04,7.00641E-03,2.60890E-03,1.11548E-04,3.49927E-04,8.63311E-05,2.07981E-04,1.26880E-04,2.16071E-04,2.24028E-04,1.26943E-04,3.17475E-04,1.24166E-04,2.71878E-04,9.37261E-07,9.50314E-07,3.47739E-06,1.57294E-04,6.79509E-05,4.04699E-05,5.13489E-06,8.56466E-06,7.55943E-06,7.33429E-05,5.27248E-06,4.94736E-06,2.63837E-06]
        if self.name == 'plasma':
            return [0.5, 0.5]
        if self.name == 'Tungsten':
            return [1.6733E-02,9.0295E-03,1.9322E-02,1.7933E-02]
        else:
            print('material mass fraction not found, current materials available are ',self.is_this_available)

    @property
    def serpent_material_card_zaid(self):
        try:
            material_card = 'mat ' + self.name + ' -' + str(self.density_g_per_cm3) + '\n'
        except:
            material_card = 'mat ' + self.name + ' ' + str(self.atom_density_per_barn_per_cm) + '\n'

        for zaid, isotopes_mass_fraction in zip(self.zaids, self.isotopes_mass_fractions):
            #str_zaid = str(zaid).zfill(6)
            if zaid.startswith('160'):
                material_card = material_card + (zaid + '.03c ' + str(isotopes_mass_fraction) + '\n')
            else:
                material_card = material_card + (zaid + '.31c ' + str(isotopes_mass_fraction) + '\n')

        return material_card

    @property
    def description(self):

        list_of_enriched_isotope_keys = []
        if self.enriched_isotopes:
            list_of_enriched_isotope_keys.append('_')
            for enriched_isotope in self.enriched_isotopes:
                for entry in enriched_isotope:
                    list_of_enriched_isotope_keys.append(
                        entry.symbol + str(entry.atomic_number) + '_' + str(entry.abundance))

            return self.name + '_'.join(list_of_enriched_isotope_keys)
        else:
            return self.name


class Compound:
    def __init__(self, chemical_equation, packing_fraction=1, theoretical_density=1, pressure_Pa=8.0E6,temperature_K=823.0, enriched_isotopes='Natural'):
        self.chemical_equation = chemical_equation
        self.list = [a for a in re.split(r'([A-Z][a-z]*)', chemical_equation) if a]
        self.enriched_isotopes = enriched_isotopes
        self.packing_fraction = packing_fraction
        self.theoretical_density = theoretical_density
        self.pressure_Pa = pressure_Pa
        self.temperature_K = temperature_K

    @property
    def density_g_per_cm3_idea_gas(self):
        molar_mass = Element('He').molar_mass_g #4.002602
        density_kg_m3 = (self.pressure_Pa / (8.31 * self.temperature_K)) * molar_mass * 6.023e23 * 1.66054e-27
        return density_kg_m3

    @property
    def is_this_available(self):
        if self.chemical_equation in ['Li4SiO4', 'Li2SiO3', 'Li2ZrO3', 'Li2TiO3', 'Be', 'Be12Ti', 'Ba5Pb3', 'Nd5Pb4',
                                      'Zr5Pb3', 'Zr5Pb4']:
            return True
        else:
            return False

    @property
    def available_comounds(self):
        return ['Li4SiO4', 'Li2SiO3', 'Li2ZrO3', 'Li2TiO3', 'Be', 'Be12Ti', 'Ba5Pb3', 'Nd5Pb4','Zr5Pb3', 'Zr5Pb4']

    @property
    def elements(self):

        list_elements = []
        for counter in range(0, len(self.list)):
            # print(self.list[counter])
            if not is_number(self.list[counter]):
                #print(' a letter? ',self.list[counter])
                if self.enriched_isotopes!='Natural':



                    # this stopes multiple elements enrichment, should update this
                    #print(self.enriched_isotopes)
                    #print(self.enriched_isotopes)
                    # if len(self.enriched_isotopes[0])==1:
                    #     print('number of enriched isotopes can not be 1, you must list all isotopes present in the natural material')
                    #     sys.exit()

                    # enriched_element = True
                    # for group_of_enriched_isotopes in self.enriched_isotopes:
                    #     print(group_of_enriched_isotopes.symbol)
                    #
                    #
                    if self.enriched_isotopes[0].symbol == self.list[counter]:
                    #             enriched_element = False
                    #
                    #     if enriched_element == True:
                    #         # print('adding element with enriched isotopes ',group_of_enriched_isotopes)
                        list_elements.append(Element(self.list[counter], self.enriched_isotopes))
                    else:
                    #         # print('adding element with natural isotopes ')
                        list_elements.append(Element(self.list[counter]))
                    #
                    # # print('number of enriched isotopes can not be 1, you must list all isotopes present in the natural material')
                    # # sys.exit()
                else:
                    list_elements.append(Element(self.list[counter]))
        return list_elements

    @property
    def fractions_coefficients(self):
        list_fraction = []
        for counter in range(0, len(self.list)):
            if not is_number(self.list[counter]):
                try:
                    if is_number(self.list[counter + 1]):
                        list_fraction.append(self.list[counter + 1])
                    else:
                        list_fraction.append('1')
                except:
                    list_fraction.append('1')
        return list_fraction

    @property
    def isotopes_mass_fractions(self):
        list_isotopes_mass_fraction = []
        for fractions, element in zip(self.fractions_coefficients, self.elements):
            for isotope in element.isotopes:
                list_isotopes_mass_fraction.append(isotope.abundance * float(fractions))
        # print('list_isotopes_mass_fraction',list_isotopes_mass_fraction)
        return list_isotopes_mass_fraction

    @property
    def zaids(self):
        list_of_zaids = []
        for element in self.elements:
            for isotope in element.isotopes:
                list_of_zaids.append(str(isotope.protons) + str(isotope.atomic_number).zfill(3))
                #list_of_zaids.append(str(isotope.protons).zfill(3) + str(isotope.atomic_number).zfill(3))
        return list_of_zaids

    @property
    def serpent_material_card_zaid(self):
        material_card = 'mat ' + self.description + ' -' + str(self.density_g_per_cm3) + '\n'
        for zaid, isotopes_mass_fraction in zip(self.zaids, self.isotopes_mass_fractions):
            if zaid.startswith('160'):
                material_card = material_card + zaid + '.03c ' + str(isotopes_mass_fraction) + '\n'
            else:
                material_card = material_card + zaid + '.31c ' + str(isotopes_mass_fraction) + '\n'
        return material_card

    @property
    def molar_mass_g(self):
        masses = []
        for element in self.elements:
            element_mass = 0
            for isotope in element.isotopes:
                # isotope.print_details
                # print(isotope.abundance, isotope.mass_amu)
                element_mass = element_mass + isotope.abundance * isotope.mass_amu
            masses.append(element_mass)

        cumlative_molar_mass = 0
        # print('mass, fraction')
        for mass, fraction in zip(masses, self.fractions_coefficients):
            # print(mass, fraction)
            cumlative_molar_mass = cumlative_molar_mass + (mass * float(fraction))
        # print(self.chemical_equation,' molar_mass=',cumlative_molar_mass)
        return cumlative_molar_mass

    @property
    def mass_kg(self):
        return (
               self.molar_mass_g * 1000 * 1.66054e-27 * self.packing_fraction * self.theoretical_density) / 1000  # convert moles to kg to g

    @property
    def volume_m3(self):
        units = ''
        if self.chemical_equation == 'Li4SiO4':
            vol = 1.1543  # http://materials.springer.com/isp/crystallographic/docs/sd_1404772
            units = 'nm3'
            molecules_per_unit_cell = 14.0
        if self.chemical_equation == 'Li2SiO3':
            vol = 0.23632  # http://materials.springer.com/isp/crystallographic/docs/sd_1703282
            units = 'nm3'
            molecules_per_unit_cell = 4.0
        if self.chemical_equation == 'Li2ZrO3':
            vol = 0.24479  # http://materials.springer.com/isp/crystallographic/docs/sd_1520554
            units = 'nm3'
            molecules_per_unit_cell = 4.0
        if self.chemical_equation == 'Li2TiO3':
            vol = 0.42701  # http://materials.springer.com/isp/crystallographic/docs/sd_1716489
            units = 'nm3'
            molecules_per_unit_cell = 8.0
        if self.chemical_equation == 'Be':
            vol = 0.01622  # http://materials.springer.com/isp/crystallographic/docs/sd_0261739
            units = 'nm3'
            molecules_per_unit_cell = 2.0
        if self.chemical_equation == 'Be12Ti':
            vol = 0.22724  # http://materials.springer.com/isp/crystallographic/docs/sd_0528340
            molecules_per_unit_cell = 2.0
            units = 'nm3'
        if self.chemical_equation == 'Ba5Pb3':
            vol = 1.37583  # http://materials.springer.com/isp/crystallographic/docs/sd_0528381
            molecules_per_unit_cell = 4.0
            units = 'nm3'
        if self.chemical_equation == 'Nd5Pb4':
            vol = 1.06090  # http://materials.springer.com/isp/crystallographic/docs/sd_0252125
            molecules_per_unit_cell = 4.0
            units = 'nm3'
        if self.chemical_equation == 'Zr5Pb3':
            vol = 0.36925  # http://materials.springer.com/isp/crystallographic/docs/sd_0307360
            molecules_per_unit_cell = 2.0
            units = 'nm3'
        if self.chemical_equation == 'Zr5Pb4':
            vol = 0.40435  # http://materials.springer.com/isp/crystallographic/docs/sd_0451962
            molecules_per_unit_cell = 2.0
            units = 'nm3'
        if self.chemical_equation == 'Pb84.2Li15.8':
            atoms_per_barn_cm2 = 3.2720171E-2
            atoms_per_cm3 = atoms_per_barn_cm2 / 1e-24
            units = 'cm3'
            vol = 1 / atoms_per_cm3
            molecules_per_unit_cell = 1.0 / 100.0
            # print('unit cell volume ='+str(vol)+' '+units)
            # if units=='nm3':
            #   vol=(vol*1.0e-21)/molecules_per_unit_cell
            # convert nm3 to cm3
        if units == 'nm3':
            vol = (vol * 1.0e-27) / molecules_per_unit_cell
        if units == 'cm3':
            vol = (vol * 1.0e-6) / molecules_per_unit_cell
        if units != '':
            return vol
        else:
            print('Compoound volume not found for ', self.chemical_equation)
            sys.exit()

    @property
    def description(self):

        list_of_enriched_isotope_keys = []
        if self.enriched_isotopes!='Natural':
            list_of_enriched_isotope_keys.append('_')
            #print('self.enriched_isotopes',self.enriched_isotopes)
            #print('self.enriched_isotopes',self.chemical_equation)
            for enriched_isotope in self.enriched_isotopes:
                #for entry in enriched_isotope:
                 list_of_enriched_isotope_keys.append(enriched_isotope.symbol + str(enriched_isotope.atomic_number) + '_' + str(enriched_isotope.abundance))

        return self.chemical_equation + '_'.join(list_of_enriched_isotope_keys)

    @property
    def density_kg_per_m3(self):

        if self.chemical_equation == 'He':
            density = self.density_g_per_cm3_idea_gas
        else:
            density = self.mass_kg / self.volume_m3
        return density

    @property
    def density_g_per_cm3(self):
        density = self.density_kg_per_m3 * 0.001
        return density





class Homogenised_mixture:
    def __init__(self, compound1, volume_fraction1, compound2, volume_fraction2):
        self.compound1 = compound1
        self.compound2 = compound2
        if volume_fraction1 > 0 and volume_fraction1 < 1.0 and volume_fraction2 > 0 and volume_fraction2 < 1.0 and volume_fraction1+volume_fraction2 == 1.0:
            self.volume_fraction1 = volume_fraction1
            self.volume_fraction2 = volume_fraction2
        else:
            print('Volume fractions must be between 0 and 1 and add upto 1')
            sys.exit()

    @property
    def density_g_per_cm3(self):
        return (self.compound1.density_g_per_cm3 * self.volume_fraction1) + (self.compound2.density_g_per_cm3 * self.volume_fraction2)

    @property
    def serpent_material_card_zaid(self):

        material_card='mat '+self.description
        material_card = material_card + '  -' + str(self.density_g_per_cm3) + '\n'
        for compound, volume_fraction in zip([self.compound1,self.compound2],[self.volume_fraction1,self.volume_fraction2]):
            for zaid, isotopes_mass_fraction in zip(compound.zaids, compound.isotopes_mass_fractions):
                if zaid.startswith('160'):
                    material_card = material_card + (zaid + '.03c ' + str(isotopes_mass_fraction*volume_fraction) + '\n')
                else:
                    material_card = material_card + (zaid + '.31c ' + str(isotopes_mass_fraction*volume_fraction) + '\n')

        return material_card

    @property
    def description(self):

        description_to_return=''
        for compound in [self.compound1,self.compound2]:
            description_to_return = description_to_return+ compound.description
            # if compound.enriched_isotopes!='Natural':
            #     print('compound.enriched_isotopes',compound.enriched_isotopes)
            #     for enriched_isotopes in compound.enriched_isotopes:
            #         description_to_return = description_to_return +'_'+ enriched_isotopes.symbol + str(enriched_isotopes.atomic_number) + '_' + str(enriched_isotopes.abundance)
            description_to_return = description_to_return+ '_vf_' + str(self.volume_fraction1) + '_'


        return description_to_return



if __name__ == "__main__":

    # #plain isotopes
    # iso1 = Isotope('Li', 6)
    # iso2 = Isotope(3, 6)
    # print(iso1.description)
    # print(iso2.description)
    #
    # #plain material
    # mat =Material('Eurofer')
    # print(mat.name)
    # print(mat.isotopes)
    # print(mat.density_g_per_cm3)
    # print(mat.isotopes_mass_fractions)
    # print(mat.serpent_material_card_zaid)
    #
    # #plain compound
    # mat = Compound('Li4SiO4')
    # print(mat.serpent_material_card_zaid)
    #
    # #enriched compound
    # mat = Compound('Li4SiO4',enriched_isotopes=(Isotope('Li', 6, 0.9), Isotope('Li', 7, 0.1)))
    # print(mat.serpent_material_card_zaid)
    #
    # #pebble bed compound
    # mat_pebble = Compound('Be12Ti', packing_fraction=0.64)
    # mat_solid = Compound('Be12Ti')
    # print('density pebble ',mat_pebble.density_g_per_cm3)
    # print('density solid ',mat_solid.density_g_per_cm3)
    #
    # #gaseous compound
    # mat = Compound('He', pressure_Pa = 8.0E6, temperature_K = 823.0)
    # print('density of high temperature high pressure ideal gas = ',mat.density_g_per_cm3_idea_gas)

    #mixed compound
    ortho = Compound('Li4SiO4',packing_fraction=0.64,  enriched_isotopes=(Isotope('Li', 6, 0.9), Isotope('Li', 7, 0.1)))
    meta = Compound('Li2TiO3',packing_fraction=0.64,  enriched_isotopes=(Isotope('Li', 6, 0.9), Isotope('Li', 7, 0.1)))
    beti = Compound('Be12Ti', packing_fraction=0.64)
    be = Compound('Be', packing_fraction=0.64)
    
    print(ortho.serpent_material_card_zaid)
    print(meta.serpent_material_card_zaid)
    print(beti.serpent_material_card_zaid)
    print(be.serpent_material_card_zaid)

    # #plain material
    # mat = Material('Eurofer')
    # print(mat.serpent_material_card_zaid)
    #
    # #mixed material
    # mat1 = Material('Eurofer')
    # mat2 = Compound('He', pressure_Pa = 8.0E6, temperature_K = 823.0)
    # hom_mat = Homogenised_mixture(mat1, 0.8, mat2, 0.2)
    # print(hom_mat.serpent_material_card_zaid)

