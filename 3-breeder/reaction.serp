set title "spheromak" % a *very* rough approximation of a fusion reactor

% surfaces
surf 1 sph 0 0 0 100 % sphere, centered at origin with R=100cm
surf 2 sph 0 0 0 ??? % please fill in your own radii
surf 3 sph 0 0 0 ???
surf 4 sph 0 0 0 ???
surf 5 sph 0 0 0 ???

% cells
cell 1 0 vacuum      -1    % when R<100cm, material is vacuum and neutrons are unimpeded
cell 2 0 ???          1 -2  % materials need assigning here...
cell 3 0 ???          2 -3 
cell 4 0 lithium-orthosilicate 3 -4 % breeder region
cell 5 0 ???          4 -5 
cell 6 0 outside      5    % neutron graveyard

% plot geometry
plot 3 800 800 % xy-plane, image dimensions (px)

% material definition - you can define some of your own too!
mat tungsten -19.25 % g cm^-3
  74182.31c -0.2650 % weight fraction
  74183.31c -0.1431
  74184.31c -0.3064
  74186.31c -0.2843
mat water -1.0 % g cm^-3
  1001.31c 2 % atom fraction
  8016.31c 1
mat vacuum -0.000001 % low density void region (having material defined allows track plot)
  7014.31c 1
  8016.31c 4
mat lithium-orthosilicate -1.5019708283669584
  3006.31c 3.6
  3007.31c 0.4
  14028.31c 0.92223
  14029.31c 0.04685
  14030.31c 0.03092
  8016.31c 4.0
mat lithium-metatitanate -2.151960376432712
  3006.31c 1.8
  3007.31c 0.2
  22046.31c 0.0825
  22047.31c 0.0744
  22048.31c 0.7372
  22049.31c 0.0541
  22050.31c 0.0518
  8016.31c 3.0
mat beryllium-titanium -1.4592683275843523
  4009.31c 12.0
  22046.31c 0.0825
  22047.31c 0.0744
  22048.31c 0.7372
  22049.31c 0.0541
  22050.31c 0.0518
mat beryllium -1.1809689147624252
  4009.31c 1.0
mat lithium-lead -9.5 % g cm^-3, 90% Li-6 enrichment
  3006.31c  1.42028E-01 % atomic fraction
  3007.31c  1.58032E-02
  82206.31c 2.15043E-01
  82207.31c 1.86037E-01
  82208.31c 4.41088E-01
mat steel -9 % g cm^-3
  26054.31c  4.3367E-03 % atom fraction 
  26056.31c  6.8561E-02  
  26057.31c  1.6452E-03  
  26058.31c  2.2431E-04  
   6012.31c  8.8179E-05  
  25055.31c  3.3592E-04  
  15031.31c  4.1990E-06  
  16032.31c  4.1990E-06  
  14028.31c  4.0025E-05  
  14029.31c  1.9668E-06  
  28058.31c  5.7173E-06  
  28060.31c  2.2019E-06  
  28061.31c  9.5737E-08  
  28062.31c  3.0527E-07  
  28064.31c  7.7765E-08  
  24050.31c  3.2844E-04  
  24052.31c  6.3329E-03  
  24053.31c  7.1811E-04  
  24054.31c  1.7879E-04  
  42092.31c  6.2313E-07  
  42094.31c  3.8841E-07  
  42095.31c  6.6848E-07  
  42096.31c  7.0039E-07  
  42097.31c  4.1780E-07  
  42098.31c  1.0128E-06  
  42100.31c  4.0436E-07  
  23051.31c  1.6796E-04  
  73181.31c  1.0078E-04  
  74182.31c  2.4480E-04  
  74183.31c  1.3218E-04  
  74184.31c  2.8301E-04  
  74186.31c  2.6260E-04  
  22046.31c  1.3857E-06  
  22047.31c  1.2496E-06  
  22048.31c  1.2382E-05  
  22049.31c  9.0866E-07  
  22050.31c  8.7003E-07  
  29063.31c  5.8089E-06  
  29065.31c  2.5891E-06  
  41093.31c  4.1990E-06  
  13027.31c  8.3980E-06  
   7014.31c  2.5194E-05  
   5010.31c  3.3424E-07  
   5011.31c  1.3454E-06  
  27059.31c  8.3980E-06  
%  50116.31c  1.22E-06
%  50117.31c  6.45E-07
  50118.31c  2.03E-06
  50119.31c  7.21E-07
  50120.31c  2.74E-06
  50122.31c  3.89E-07
  50124.31c  4.86E-07
  40090.31c  8.641E-06
  40091.31c  1.884E-06
  40092.31c  2.880E-06
  40094.31c  2.919E-06
  40096.31c  4.703E-07
   8016.31c  8.3980E-06  
% add other materials here...

% src definition
src dt-plasma  % name
  se 14.1 % energy in MeV
  sc 1 % start neutrons in the 'plasma' cell
set nps 10000 10 % neutron population, bunch count
set srcrate 1

det tbr % detector example, this tallies tritium production per source neutron
  dc 4 % cell number/name
  dr -55 lithium-orthosilicate % 55 is the tritium production tally

% cross-section data
set acelib "/opt/serpent2/xsdir.serp"

% reduce memory consumption
set opti 1
